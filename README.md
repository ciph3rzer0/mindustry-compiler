# Mindustry

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.4.

It uses the Monaco editor (angular package: [nge-monaco](https://github.com/mciissee/nge-monaco)) to provide a code editor with syntax highlighting and other modern features.  It's the same editor used in VS Code.

Antlr4 parses a higher level language and converts it to [Mindustry](https://anuke.itch.io/mindustry) [logic code](https://mindustrygame.github.io/wiki/logic/0-introduction/).

Firestore (database-as-a-service) is used to store code in user accounts.
Firebase is used for hosting and user authentication.

NgRx/Data is used to fetch and cache data from firestore.

Clarity Design Systems is used for some UI elements.


## Development server

Run `ng serve` for a dev server. Your  `http://localhost:4200/`. The app will automatically reload if you change any of the source files.  This will connect to the firestore database.

### Local emulated environment
In two separate tabs, run `npm run emulator` and `npm run local`.  This creates a local firebase emulator that saves on close.  The emulated firestore console will be available at `http://localhost:4000/`, where you can adjust firestore data and authenticated users.  The dev server will be available at `http://localhost:4200/`

## Build

Run `npm run antlr` to first compile the antlr files.  This only needs done once unless you plan on changing the MinLang grammar.

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

### Deploying to production:
https://firebase.google.com/docs/cli#partial_deploys

#### Deploy hosting
`ng build --prod`.
`firebase deploy --only hosting`
#### Deploy functions
`firebase deploy --only functions`
#### Deploy security rules:
`firebase deploy --only firestore:rules`

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

### Testing Cloud Functions
Additional tests for Cloud Functions are available in the `functions/` directory.  This spins up an empty firstore database and loads the Cloud Functions environment.  See that ReadMe for more info.

## Running end-to-end tests
Never set up.

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

