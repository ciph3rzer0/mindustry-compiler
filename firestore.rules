rules_version = '2';
service cloud.firestore {
  match /databases/{database}/documents {
    function isLoggedIn() {
      return request.auth != null;
    }
    function isPublic() {
      return resource != null && 'vis' in resource.data && resource.data.vis == 'public';
    }
    function isPrivate() {
      return resource != null && 'vis' in resource.data && resource.data.vis == 'private';
    }
    function isDocOwner() {
      return isLoggedIn() && request.auth.uid == resource.data.uid;
    }
    function isUidValid() {
      return isLoggedIn() && request.auth.uid == request.resource.data.uid;
    }

    // Disallow by default
    match /{document=**} {
      allow read: if false;
      allow write: if false;
    }

    match /codefiles/{id} {
      allow create: if isLoggedIn() && isUidValid();
      allow write: if isDocOwner() &&  isUidValid();
      allow read: if !isPrivate() || isDocOwner();
    }

    // Version history cannot be edited directly.
    match /codefiles/{id}/_history/{vid} {
      allow write: if false;
      allow read: if !isPrivate();
    }
  }
}