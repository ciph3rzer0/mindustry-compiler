How to test functions:

http://localhost:5001/mindustry-compiler/us-central1/addMessage?text=uppercase


If the emulator continues running, use these commands in PowerShell:

```powershell
Get-Process -Id (Get-NetTCPConnection -LocalPort 8080).OwningProcess
Stop-Process #id
```

# Running Unit Tests
`npm run test` to start the emulator, run the tests, and shut down.

`npm run serve` will spin up the emulated firebase database indefinitely; allowing you to run `npm run jest` in another console window.  Use this if you are developing a function to save time spinning up before each test.