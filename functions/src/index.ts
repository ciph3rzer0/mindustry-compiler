import * as admin from 'firebase-admin';
import * as functions from 'firebase-functions';
import { gt, inc, neq, valid } from 'semver';
import 'source-map-support/register';

import FieldValue = admin.firestore.FieldValue;

admin.initializeApp();

const db = admin.firestore();
const ffd = functions.firestore.document;
const HISTORY = '_history';
const VERSION = 'version';

// export const createCodeFile = ffd('codefiles/{id}').onCreate(
//   async (snapshot, context) => {
//     const version = snapshot.get(VERSION);
//     if (!valid(version)) {
//       functions.logger.error(
//         `Version '${version}' is invalid.  Defaulting to '0.0.1'`
//       );

//       db.doc(snapshot.ref.path).set({
//         ...snapshot.data(),
//         version: '0.0.1',
//       });
//     }
//   }
// );

/**
 * Creates a version history of the document in the _history subcollection.
 * 
 * Does semver checks but doesn't act on all of them.  Ideally it would strictly
 * enforce versions but I need to rethink the database structure a bit to make
 * it work.
 */
export const updateCodeFile = ffd('codefiles/{id}').onUpdate(
  async (change, context) => {
    const id = change.before.id;
    const oldFile = change.before.data();

    let vBefore = change.before.get(VERSION);
    let vAfter = change.after.get(VERSION);

    functions.logger.info(
      `Updating '${oldFile.docName}' (${id}).  Version ${vBefore} to ${vAfter}`
    );

    if (!valid(vBefore)) {
      functions.logger.error('Invalid previous version: ', vBefore);
      vBefore = null;
    } else if (!valid(vAfter)) {
      functions.logger.error('Invalid update version: ', vAfter);
      vAfter = inc(vBefore, 'patch');
    } else if (gt(vBefore, vAfter)) {
      functions.logger.error('Improper version sequencing.');
      vAfter = inc(vBefore, 'patch');
    }

    // Verify this version is greater than the previous.
    const latestDocVersion = await change.after.ref
      .collection(HISTORY)
      .orderBy('date', 'desc')
      .limit(1)
      .get();

    if (latestDocVersion.docs.length === 1) {
      const vLast = latestDocVersion.docs[0].get(VERSION);
      if (vBefore === null) vBefore = inc(vLast, 'patch');

      if (gt(vLast, vBefore)) {
        functions.logger.error(
          `Last revision (${vLast}) was greater than updated document (${vBefore})`
        );
      } else {
        functions.logger.debug(
          `OK - Last revision (${vLast}) less than (${vBefore})`
        );
      }
    } else {
      functions.logger.debug('No previous version was found');
      if (vBefore === null) vBefore = '0.0.0';
    }

    const historicalVersion = {
      doc: oldFile.doc,
      version: vBefore,
      date: FieldValue.serverTimestamp(),
    };

    try {
      functions.logger.info(`Writing revision to ${id}/_history/${vBefore}`);
      await change.after.ref
        .collection(HISTORY)
        .doc(vBefore)
        .create(historicalVersion);
    } catch (err) {
      functions.logger.error(
        'error writing version history',
        change.after.ref.path,
        err
      );
    }

    // If the updated version was improper, set the document again.
    const originalAfterVersion = valid(change.after.get(VERSION));
    if (!originalAfterVersion || neq(vAfter, originalAfterVersion)) {
      functions.logger.error(
        `Updated version was improper; Ignoring.  But ideally we update ` +
          `${change.after.get(VERSION)} -> ${vAfter}.`
      );
    }
  }
);
