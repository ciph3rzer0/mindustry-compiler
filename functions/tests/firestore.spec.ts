import * as firebase from '@firebase/rules-unit-testing';
import * as admin from 'firebase-admin';

import { Codefile } from '../../src/app/data/models/codefile.model';

const projectId = 'mindustry-compiler';
process.env.GCLOUD_PROJECT = projectId;
process.env.FIRESTORE_EMULATOR_HOST = 'localhost:8080';

expect.extend({
  async toAllow(x) {
    let pass = false;
    try {
      await firebase.assertSucceeds(x);
      pass = true;
    } catch (err) {}

    return {
      pass,
      message: () =>
        'Expected Firebase operation to be allowed, but it was denied',
    };
  },
});

expect.extend({
  async toDeny(x) {
    let pass = false;
    try {
      await firebase.assertFails(x);
      pass = true;
    } catch (err) {}
    return {
      pass,
      message: () =>
        'Expected Firebase operation to be denied, but it was allowed',
    };
  },
});

declare global {
  namespace jest {
    interface Matchers<R> {
      toAllow: () => boolean;
      toDeny: () => boolean;
    }
  }
}

const sleep = (waitTimeInMs = 2000) =>
  new Promise((resolve) => setTimeout(resolve, waitTimeInMs));

const stageMockData = async (data) => {
  // Write mock documents as admin
  const adminApp = await firebase.initializeAdminApp({
    projectId,
  });
  const adminDb = adminApp.firestore();

  if (data) {
    for (const key in data) {
      const ref = adminDb.doc(key);
      await ref.set(data[key]);
    }
  }
};

const setup = async (auth) => {
  const app = await firebase.initializeTestApp({
    projectId,
    auth,
  });

  return app.firestore();
};

const mockData = {
  'codefiles/file1': {
    doc: `print("HI THERE WORLD!");\n`,
    docName: `file 1`,
    uid: `user1`,
    version: `1.0.0`,
  },
  'codefiles/file2': {
    doc: `let a = 1;\n`,
    docName: `file 2`,
    uid: `user2`,
    version: `3.1.4`,
  },
  'codefiles/directFileVersion': {
    doc: `are belong to us`,
    docName: `directFileVersion`,
    uid: `user1`,
    version: `1.0.1`,
  },
  'codefiles/directFileVersion/_history/1.0.0': {
    doc: `all your base`,
    docName: `history test`,
    uid: `user1`,
    version: `1.0.0`,
  },
  'codefiles/privateFile': {
    doc: 'secret();\n',
    docName: `private doc`,
    uid: 'privateUser',
    version: 'x.x.x',
    vis: 'private',
  },
  'codefiles/invalidUpdate': {
    doc: `let y = mx + b;\n`,
    docName: `Invalid Update`,
    uid: `user1`,
    version: `2.0.1`,
  },
};

describe(`Cloud Functions`, () => {
  afterAll(async () => {
    await firebase.firestore().terminate();
    await Promise.all(firebase.apps().map((app) => app.delete()));
  });

  beforeAll(async () => {
    await firebase.clearFirestoreData({ projectId });
    await stageMockData(mockData);
  });

  it(`should not allow default access to random docs`, async () => {
    const db = await setup({ uid: 'guest' });
    let projRef = db.doc('projects/testId');

    await expect(projRef.get()).toDeny();
  });

  it(`should allow a registered user to add a codefile`, async () => {
    const db = await setup({ uid: 'veryCoolDude' });

    let newCodefile = {
      id: 'newCodefile1',
      uid: 'veryCoolDude',
      doc: `print("hello world")`,
    };

    let codefiles = db.collection('codefiles');
    let docRef = codefiles.doc(newCodefile.id);
    await expect(docRef.set(newCodefile)).toAllow();

    let doc: any = await codefiles.doc(newCodefile.id).get();
    expect(doc).toBeDefined();
    expect(doc.data().uid).toEqual(newCodefile.uid);
  });

  it(`should deny a guest user to add a codefile`, async () => {
    const db = await setup(null);
    let newFile = {
      id: 'newCodefile1',
      uid: null,
      doc: `print("hello world")`,
    };

    let codefiles = db.collection('codefiles');
    let docRef = codefiles.doc(newFile.id);
    await expect(docRef.set(newFile)).toDeny();
  });

  it(`should enforce UID accuracy when creating a file`, async () => {
    const db = await setup({ uid: 'ordinaryUser' });
    let newFile = {
      id: 'hackedCodefile',
      uid: 'admin',
      doc: `I'M A HAXOR LOL`,
    };

    let codefiles = db.collection('codefiles');
    let docRef = codefiles.doc(newFile.id);
    await expect(docRef.set(newFile)).toDeny();
  });

  it(`should enforce UID accuracy when updating a file`, async () => {
    const db = await setup({ uid: 'user1' });
    const filename = 'codefiles/file1';

    let newFile = {
      ...mockData[filename],
      uid: 'anotherUser',
    };

    await expect(db.doc(filename).update(newFile)).toDeny();
  });

  it(`should allow user to read and write their own codefile`, async () => {
    const db = await setup({ uid: 'user2' });
    const filename = 'codefiles/file2';

    await expect(db.doc(filename).get()).toAllow();
    await expect(
      db.doc(filename).update({ id: filename, version: '4.0.0' })
    ).toAllow();

    let updatedDoc = await db.doc(filename).get();
    expect(updatedDoc.get('version')).toEqual('4.0.0');
  });

  it(`should deny writes to someone elses codefile`, async () => {
    const db = await setup({ uid: 'deadbeatUser' });
    const filename = 'codefiles/file1';

    await expect(db.doc(filename).get()).toAllow();
    await expect(
      db.doc(filename).update({ id: filename, doc: 'crap' })
    ).toDeny();
  });

  it(`should deny reading private codefile versions`, async () => {
    const db = await setup(null);

    await expect(db.doc('codefiles/privateFile').get()).toDeny();
  });

  it(`should deny user ability to edit versions directly`, async () => {
    const db = await setup({ uid: `user1` });

    await expect(
      db.doc('codefiles/directFileVersion/_history/1.0.0').update({
        doc: `stealth update`,
      })
    ).toDeny();
  });

  it(`should save a version history when updated`, async () => {
    const db = await setup({ uid: 'user1' });
    const filename = 'codefiles/file1';
    const codefileReference = db.doc(filename);

    const version_1_0_0 = {
      ...mockData[filename],
    };

    const version_1_0_1 = {
      ...mockData[filename],
      doc: `print("Hello World");\n`,
      version: '1.0.1',
    };

    const version_1_0_2 = {
      ...mockData[filename],
      doc: `print("Hello Universe");\n`,
      version: '1.0.2',
    };

    /** First version update */
    await expect(codefileReference.update(version_1_0_1)).toAllow();
    let firstUpdate = await codefileReference.get();

    // TODO: eventually store metadata about versions history
    // expect(firstUpdate.data()._versions_summary).toBeDefined();
    expect(firstUpdate.data().version).toEqual(version_1_0_1.version);
    expect(firstUpdate.data().doc).toEqual(version_1_0_1.doc);

    await sleep();
    let query = await codefileReference
      .collection('_history')
      .orderBy('date', 'desc')
      .limit(10)
      .get();

    expect(query).toBeDefined();
    expect(query.docs.length).toEqual(1);
    expect(query.docs[0].data().doc).toEqual(version_1_0_0.doc);
    expect(query.docs[0].data().version).toEqual(version_1_0_0.version);

    /** Second version update */
    await expect(codefileReference.update(version_1_0_2)).toAllow();
    let secondUpdate = await codefileReference.get();
    expect(secondUpdate.data().version).toEqual(version_1_0_2.version);
    expect(secondUpdate.data().doc).toEqual(version_1_0_2.doc);

    await sleep();
    query = await codefileReference
      .collection('_history')
      .orderBy('date', 'desc')
      .limit(10)
      .get();

    expect(query).toBeDefined();
    expect(query.docs.length).toEqual(2);
    expect(query.docs[0].data().doc).toEqual(version_1_0_1.doc);
    expect(query.docs[0].data().version).toEqual(version_1_0_1.version);
    expect(query.docs[1].data().doc).toEqual(version_1_0_0.doc);
    expect(query.docs[1].data().version).toEqual(version_1_0_0.version);
  });

  it(`should change an invalid version to 0.0.1`, async () => {
    const db = await setup({ uid: 'privateUser' });
    let filename = 'codefiles/privateFile';

    await sleep();
    let file = await db.doc(filename).get();

    expect(file.data().version).toBe('0.0.1');
  });

  // I can't do this without triggering another update version and historical
  // archive.  Should either update with cloud functions or provide a path
  // to update in a "correction" mode that doesn't trigger the usual update mechanics.
  // Enforce that the correction field is blank in firebase rules to prevent abuse.
  // xit(`should change an invalid update to the next patch version`, async () => {
  it(`should should still update with invalid version`, async () => {
    const db = await setup({ uid: 'user1' });
    let filename = 'codefiles/invalidUpdate';

    await expect(
      db.doc(filename).set({
        ...mockData[filename],
        version: 'bad version',
      })
    ).toAllow();

    await sleep();
    let file = await db.doc(filename).get();

    // expect(file.data().version).toBe('2.0.2');
    expect(file.data().version).toBe('bad version');
  });
});
