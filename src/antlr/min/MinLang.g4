grammar MinLang;
// Based on: https://stackoverflow.com/questions/15610183/if-else-statements-in-antlr-using-listeners

parse
 : block EOF
 ;

block
 : stat* return_stat?
 ;

stat
 : assignment
 | if_stat
 | while_stat
 | declare_inline_function
 | function_expr SCOL
 | native_instruction
 | instruction
 | OTHER {console.log("unknown char: " + $OTHER.text);}
 ;

assignment
 : ID ASSIGN expr SCOL
 ;

if_stat
 : IF condition_block (ELSE IF condition_block)* (ELSE stat_block)?
 ;

condition_block
 : expr stat_block
 ;

stat_block
 : OBRACE block CBRACE
 | stat
 ;

while_stat
 : WHILE expr stat_block
 ;

declare_inline_function
 : INLINE ID parameters stat_block 
 ;

parameters
 : OPAR (ID ( COMMA ID )*)? CPAR
 ;
return_stat
 : RETURN expr SCOL?
 ;

function_expr
 : ID arguments
 ;

// method
//  : ID OPAR expr CPAR stat_block
//  ;

instr
 : READ
 | WRITE
 | DRAW
 | PRINT
 | DRAW_FLUSH
 | PRINT_FLUSH
 | GETLINK
 | CONTROL
 | RADAR
 | SENSOR
 | END
 | SET
 | OP
 | JUMP
 | UNIT_BIND
 | UNIT_CONTROL
 | UNIT_RADAR
 | UNIT_LOCATE
 ;

native_instruction
 : instr args=spaced_arguments SCOL?
 ;

instruction
 : READ arguments SCOL                      #readInstruction
 | WRITE arguments SCOL                     #writeInstruction
 | DRAW arguments SCOL                      #drawInstruction
 | PRINT arguments SCOL                     #printInstruction
 | DRAW_FLUSH arguments SCOL                #drawFlushInstruction  
 | PRINT_FLUSH arguments SCOL               #printFlushInstruction  
 | GETLINK arguments SCOL                   #linkInstruction
 | CONTROL arguments SCOL                   #controlInstruction
 | RADAR arguments SCOL                     #radarInstruction
 | SENSOR arguments SCOL                    #sensorInstruction
 | END arguments SCOL                       #endInstruction
 | UNIT_BIND arguments SCOL                 #unitBindInstruction
 | UNIT_CONTROL arguments SCOL              #unitControlInstruction
 | UNIT_RADAR arguments SCOL                #unitRadarInstruction
 | UNIT_LOCATE arguments SCOL               #unitLocateInstruction
 ;

expr
 : expr POW<assoc=right> expr               #powExpr
 | MINUS expr                               #unaryMinusExpr
 | NOT expr                                 #notExpr
 | expr op=(MULT | DIV | INTDIV | MOD) expr #multiplicationExpr
 | expr op=(PLUS | MINUS) expr              #additiveExpr
 | expr op=(LTEQ | GTEQ | LT | GT) expr     #relationalExpr
 | expr op=(EQ | NEQ) expr                  #equalityExpr
 | expr AND expr                            #andExpr
 | expr OR expr                             #orExpr
 | math_function arguments                  #mathFuncExpr
 | OPAR expr CPAR                           #parExpr
 | atom                                     #atomExpr
 | function_expr                            #functionExpr
 ;

arguments 
 : OPAR expr ( COMMA expr )* CPAR
 | OPAR CPAR
 ;
 
spaced_arguments
 : atom ( atom )*
 | // nothing
 ;


math_function
 : SIN
 | COS
 | TAN
 | FLOOR
 | CEIL
 | SQRT
 | RAND
 | ABS
 | LOG
 | LOG10
 | FLIP
 | MAX
 | MIN
 | ATAN2
 | DST
 | NOISE
 ;

atom
 : (INT | FLOAT)                            #numberAtom
 | (TRUE | FALSE)                           #booleanAtom
 | ID                                       #idAtom
//  | CONST                                    #constAtom
 | STRING                                   #stringAtom
 | NULL                                     #nullAtom
 ;

OR : '||';
AND : '&&';
EQ : '==';
NEQ : '!=';
GT : '>';
LT : '<';
GTEQ : '>=';
LTEQ : '<=';
PLUS : '+';
MINUS : '-';
MULT : '*';
DIV : '/';
INTDIV : '//';
MOD : '%';
POW : '^';
NOT : '!';

COMMA: ',';
SCOL : ';';
ASSIGN : '=';
OPAR : '(';
CPAR : ')';
OBRACE : '{';
CBRACE : '}';

TRUE : 'true';
FALSE : 'false';
NULL : 'null';
IF : 'if';
ELSE : 'else';
WHILE : 'while';
INLINE : 'inline';
RETURN : 'return';

// Commands
READ : 'read';
WRITE : 'write';
DRAW : 'draw';
PRINT : 'print';
DRAW_FLUSH : 'drawflush';
PRINT_FLUSH : 'printflush';
GETLINK : 'getlink';
CONTROL : 'control';
RADAR : 'radar';
SENSOR : 'sensor';
SET : 'set';
OP : 'op';
END : 'end';
JUMP : 'jump';
UNIT_BIND : 'ubind';
UNIT_CONTROL : 'ucontrol';
UNIT_RADAR : 'uradar';
UNIT_SENSOR : 'usensor';
UNIT_LOCATE : 'ulocate';

// math functions
SIN : 'sin';
COS : 'cos';
TAN : 'tan';
FLOOR : 'floor';
CEIL : 'ceil';
SQRT : 'sqrt';
RAND : 'rand';
ABS : 'abs';
LOG : 'log';
LOG10 : 'log10';
FLIP : 'flip';

MAX : 'max';
MIN : 'min';
ATAN2 : 'atan2';
DST : 'dst';
NOISE : 'noise';

ID // TODO: create rules for the prefixes (constants and symbols)
 : ('@'|'~'|':')?[a-zA-Z_] [a-zA-Z_0-9]*
 ;

// CONST
//  : AT?[a-zA-Z_] [a-zA-Z_0-9]*
//  ;

INT
 : [0-9]+
 ;

FLOAT
 : [0-9]+ '.' [0-9]* 
 | '.' [0-9]+
 ;

STRING
 : '"' (~["\r\n] | '""')* '"'
 ;

COMMENT
 : '#' ~[\r\n]* -> skip
 ;

// NEWLINE
//  : '\r\n'
//  | '\r'
//  | '\n'
//  ;
// SPACES 
//  : [ \t]+ -> channel(CSPACE)
//  ;

// WHITESPACE
//  : SPACES -> skip
//  ;
SPACE
 : [ \t\r\n] -> skip
 ;

OTHER
 : . 
 ;