import { AbstractParseTreeVisitor } from 'antlr4ts/tree/AbstractParseTreeVisitor';
import { Declare_inline_functionContext } from './c/MinLangParser';
import { MinLangVisitor } from './c/MinLangVisitor';

// type SymMap = Map<string, any>;
type SymMap = { [key: string]: string };
type Result = SymMap;

/**
 * Not used yet but may in the future
 */
export class MinLangSymbolVisitor
  extends AbstractParseTreeVisitor<Result>
  implements MinLangVisitor<Result> {
  protected defaultResult(): Result {
    return {};
  }

  visitDeclare_inline_function(ctx: Declare_inline_functionContext): Result {
    console.log(`DeclareInline ${ctx.ID().text}`);

    let id = ctx.ID().text;
    let result = {};
    result[id] = ctx.parameters().text;
    return result;
  }
  aggregateResult(aggregate: Result, next: Result): Result {
    let result: SymMap = {};

    Object.keys(aggregate).forEach((k) => {
      result[k] = aggregate[k];
    });
    Object.keys(next).forEach((k) => {
      result[k] = next[k];
    });

    return result;
  }

  //   visitChildren(node: RuleNode): string {
  //     console.log(`children ${node.text}`);
  //     return node.text;
  //   }
}
