// / <reference path="../../../node_modules/monaco-editor/monaco.d.ts" />
import { createLexer } from './ParserFacade';
import { CommonTokenStream, CharStream, ConsoleErrorListener } from 'antlr4ts';

import ILineTokens = monaco.languages.ILineTokens;
import IToken = monaco.languages.IToken;

const lookup = {
  // blocks
  IF: 'block',
  ELSE: 'block',
  WHILE: 'block',
  RETURN: 'block',

  // methods
  READ: 'method',
  WRITE: 'method',
  DRAW: 'method',
  PRINT: 'method',
  DRAW_FLUSH: 'method',
  PRINT_FLUSH: 'method',
  GETLINK: 'method',
  CONTROL: 'method',
  RADAR: 'method',
  SENSOR: 'method',
  END: 'method',
  SET: 'method',
  OP: 'method',
  JUMP: 'method',
  UNIT_BIND: 'method',
  UNIT_CONTROL: 'method',
  UNIT_RADAR: 'method',
  UNIT_LOCATE: 'method',

  // math_functions
  SIN: 'math_function',
  COS: 'math_function',
  TAN: 'math_function',
  FLOOR: 'math_function',
  CEIL: 'math_function',
  SQRT: 'math_function',
  RAND: 'math_function',
  ABS: 'math_function',
  LOG: 'math_function',
  LOG10: 'math_function',
  FLIP: 'math_function',
  MAX: 'math_function',
  MIN: 'math_function',
  ATAN2: 'math_function',
  DST: 'math_function',
  NOISE: 'math_function',

  INLINE: 'function',
  ID: 'variable',

  // Atoms
  STRING: 'string',
  INT: 'number',
  FLOAT: 'number',
  TRUE: 'boolean',
  FALSE: 'boolean',

  OPAR: 'paren',
  CPAR: 'paren',
  COMMA: 'delimeter',
};

export class MinLangState implements monaco.languages.IState {
  clone(): monaco.languages.IState {
    return new MinLangState();
  }

  equals(other: monaco.languages.IState): boolean {
    return true;
  }
}

export class MinLangTokensProvider implements monaco.languages.TokensProvider {
  getInitialState(): monaco.languages.IState {
    return new MinLangState();
  }

  tokenize(
    line: string,
    state: monaco.languages.IState
  ): monaco.languages.ILineTokens {
    // So far we ignore the state, which is not great for performance reasons
    return tokensForLine(line);
  }
}

const EOF = -1;

class MinLangToken implements IToken {
  startIndex: number;
  scopes: string;

  constructor(ruleName: string, startIndex: number) {
    if (ruleName) {
      let rule = ruleName.toUpperCase();
      if (lookup[rule]) {
        this.scopes = lookup[rule];
      } else {
        this.scopes = ruleName.toLowerCase(); //+ '.minlang';
      }
    } else {
      this.scopes = 'undefined';
    }

    // console.log(this.scopes);
    this.startIndex = startIndex;
  }
}

class MinLangLineTokens implements ILineTokens {
  endState: monaco.languages.IState;
  tokens: monaco.languages.IToken[];

  constructor(tokens: monaco.languages.IToken[]) {
    this.endState = new MinLangState();
    this.tokens = tokens;
  }
}

export function tokensForLine(input: string): monaco.languages.ILineTokens {
  var errorStartingPoints: number[] = [];

  class ErrorCollectorListener extends ConsoleErrorListener {
    syntaxError(recognizer, offendingSymbol, line, column, msg, e) {
      errorStartingPoints.push(column);
    }
  }

  const lexer = createLexer(input);
  lexer.removeErrorListeners();
  let errorListener = new ErrorCollectorListener();
  lexer.addErrorListener(errorListener);

  let done = false;
  let myTokens: monaco.languages.IToken[] = [];
  do {
    let token = lexer.nextToken();
    if (token == null) {
      done = true;
    } else {
      // We exclude EOF
      if (token.type == EOF) {
        done = true;
      } else {
        let tokenTypeName = lexer.vocabulary.getSymbolicName(token.type);
        // console.log('>', token.type, '>', tokenTypeName, '-', token.startIndex);
        let myToken = new MinLangToken(tokenTypeName, token.startIndex); // token.column
        myTokens.push(myToken);
      }
    }
  } while (!done);

  //   TODO: Fix hack for comments
  let commentStart = input.indexOf('#');
  if (commentStart !== undefined) {
    myTokens.push(new MinLangToken('comment', commentStart));
  }

  // Add all errors
  for (let e of errorStartingPoints) {
    myTokens.push(new MinLangToken('error', e));
  }
  myTokens.sort((a, b) => (a.startIndex > b.startIndex ? 1 : -1));

  return new MinLangLineTokens(myTokens);
}
