import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MinLangLexer } from './c/MinLangLexer';
import { MinLangParser } from './c/MinLangParser';
import { MinLangSymbolVisitor } from './MinLangSymbolVisitor';
import { MinLangExecutor } from './MinLangVisitor';
import {
  MinLogicControl,
  MinLogicDraw,
  MinLogicDrawFlush,
  MinLogicEnd,
  MinLogicInstructionList,
  MinLogicJump,
  MinLogicLink,
  MinLogicNativeInstruction,
  MinLogicOperation,
  MinLogicPrint,
  MinLogicPrintFlush,
  MinLogicRadar,
  MinLogicRead,
  MinLogicReference,
  MinLogicSensor,
  MinLogicSet,
  MinLogicUnitBind,
  MinLogicUnitControl,
  MinLogicUnitLocate,
  MinLogicUnitRadar,
  MinLogicWrite,
} from './MinLogic';

// function valueCompare(first, second) {
//   if (first.value) first = first.value;
//   if (second.value) first = second.value;

//   return first == second;
// }
let customMatchers = {
  toEval: function (util, customEqualityTesters) {
    return {
      compare: (firsti, secondi) => {
        let first = firsti;
        let second = secondi;

        if (typeof first.value !== 'undefined') first = firsti.value;
        if (typeof second.value !== 'undefined') second = secondi.value;

        if (first == second) {
          return {
            pass: true,
            message: `${firsti} eval to ${secondi}`,
          };
        } else {
          return {
            pass: false,
            message: `Expected ${firsti} eval to ${secondi}`,
          };
        }
      },
    };
  },
};

declare global {
  namespace jasmine {
    interface Matchers<T> {
      toEval: (expected: any) => boolean;
    }
  }
}

describe('MinLangExecutor', () => {
  //   beforeAll(() => {});
  beforeEach(() => {
    jasmine.addMatchers(customMatchers);
    // jasmine.addCustomEqualityTester(valueCompare);
  });
  describe('Mindustry-Logic Native Passthrough', () => {
    it('should pass through native mindustry-logic instructions', () => {
      let result = parseText(`
        set i 0
        op lessThan ~t1 i 10
        jump 9 notEqual ~t1 1
        read var1 cell1 i
        write var1 cell1 i
        print "val: "
        print var1
        print "\\n"
        op add i i 1
        jump 1 always 1 1
        print "\\n"
        print @time
        printflush message1
        end
    `) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(14);
      result.forEach((i) => {
        expect(i instanceof MinLogicNativeInstruction).toBeTruthy();
      });

      expect(result.compile()).toEqual(
        `set i 0
op lessThan ~t1 i 10
jump 9 notEqual ~t1 1
read var1 cell1 i
write var1 cell1 i
print "val: "
print var1
print "\\n"
op add i i 1
jump 1 always 1 1
print "\\n"
print @time
printflush message1
end
`
      );
    });

    it('should pass through native instructions inside blocks', () => {
      let result = parseText(`
    set index 1
    if(a < b) {
        getlink @poly
        index = index + 1;
        print index
        print("\\n", @time);
        printflush display1
    }
    `) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(9);

      let rawInstr = result[0] as MinLogicNativeInstruction;
      expect(rawInstr instanceof MinLogicNativeInstruction).toBeTruthy();
      expect(rawInstr.source).toEqual('set index 1');

      let opInstr = result[1] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.type).toEval('lessThan');

      let jumpInstr = result[2] as MinLogicJump;
      expect(jumpInstr instanceof MinLogicJump).toBeTruthy();

      rawInstr = result[3] as MinLogicNativeInstruction;
      expect(rawInstr instanceof MinLogicNativeInstruction).toBeTruthy();
      expect(rawInstr.source).toEqual('getlink @poly');

      opInstr = result[4] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.type).toEval('add');

      rawInstr = result[5] as MinLogicNativeInstruction;
      expect(rawInstr instanceof MinLogicNativeInstruction).toBeTruthy();
      expect(rawInstr.source).toEqual('print index');

      let printInstr = result[6] as MinLogicPrint;
      expect(printInstr instanceof MinLogicPrint).toBeTruthy();
      expect(printInstr.what).toEval('"\\n"');

      printInstr = result[7] as MinLogicPrint;
      expect(printInstr instanceof MinLogicPrint).toBeTruthy();
      expect(printInstr.what).toEval('@time');

      rawInstr = result[8] as MinLogicNativeInstruction;
      expect(rawInstr instanceof MinLogicNativeInstruction).toBeTruthy();
      expect(rawInstr.source).toEqual('printflush display1');
    });
  });

  describe('Math Expressions', () => {
    it('should set a single variable', () => {
      let result = parseText('index = 1;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicSet);

      let result0 = result[0] as MinLogicSet;
      expect(result0.outputVar).toEval('index');
      expect(result0.setValue).toEval(1);
    });

    it('Should handle a single addition', () => {
      let result = parseText('index = index + 1;') as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let result0 = result[0] as MinLogicOperation;
      expect(result0.type).toEqual('add');
      expect(result0.outputVar).toBeTruthy();
      expect(result0.op1).toEval('index');
      expect(result0.op2).toEval(1);
    });

    it('Should handle a single subtraction', () => {
      let result = parseText('index = index - 1;') as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let instr = result[0] as MinLogicOperation;
      expect(instr.type).toEqual('sub');
      expect(instr.outputVar).toEval('index');
      expect(instr.op1).toEval('index');
      expect(instr.op2).toEval(1);
    });

    it('Should produce a negative number from unary minus', () => {
      let result = parseText('index = -1;') as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let instr = result[0] as MinLogicSet;
      expect(instr.outputVar).toEval('index');
      expect(instr.setValue).toEval(-1);
    });

    it('Should produce a subtraction op from unary minus of a compound term', () => {
      let result = parseText(
        'index = -(4 * index);'
      ) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);

      // ~t1 = (4 * index)
      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEqual('mul');
      expect(instr.op1).toEval(4);
      expect(instr.op2).toEval('index');

      instr = result[1] as MinLogicOperation;
      expect(instr.outputVar).toEval('index');
      expect(instr.type).toEqual('sub');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toBe(tempVar1);
    });

    it('Should handle a single division', () => {
      let result = parseText('index = index / 1;') as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let instr = result[0] as MinLogicOperation;
      expect(instr.type).toEqual('div');
      expect(instr.outputVar).toBeTruthy();
      expect(instr.op1).toEval('index');
      expect(instr.op2).toEval(1);
    });

    it('Should handle exponentials', () => {
      let result = parseText('index = 1 + 5^4 - 1;') as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(3);
      expect(result[0] instanceof MinLogicOperation);
      expect(result[1] instanceof MinLogicOperation);
      expect(result[2] instanceof MinLogicOperation);

      // ~t1 = 5^5
      let instr = result[0] as MinLogicOperation;
      expect(instr.type).toEqual('pow');
      expect(instr.outputVar).toBeTruthy();
      let tempVar1 = instr.outputVar;
      expect(instr.op1).toEval(5);
      expect(instr.op2).toEval(4);

      // ~t2 = 1 + ~t1
      instr = result[1] as MinLogicOperation;
      expect(instr.type).toEqual('add');
      expect(instr.outputVar).toBeTruthy();
      let tempVar2 = instr.outputVar;
      expect(instr.op1).toEval(1);
      expect(instr.op2).toBe(tempVar1);

      // ~t3 = ~t2 - 1
      instr = result[2] as MinLogicOperation;
      expect(instr.type).toEqual('sub');
      expect(instr.outputVar).toEval('index');
      expect(instr.op1).toBe(tempVar2);
      expect(instr.op2).toEval(1);
    });

    it('Should break down compound math expressions', () => {
      let result = parseText('newX = newX * 2 + 3;') as MinLogicInstructionList;

      expect(result).toBeTruthy();
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toEqual(2);

      let result0 = result[0] as MinLogicOperation;
      expect(result0.type).toEqual('mul');
      expect(result0.outputVar).toBeTruthy();
      let tempVar = result0.outputVar;
      expect(result0.op1).toEval('newX');
      expect(result0.op2).toEval(2);

      let result1 = result[1] as MinLogicOperation;
      expect(result1.type).toEqual('add');
      expect(result1.outputVar).toEval('newX');
      expect(result1.op1).toBe(tempVar);
      expect(result1.op2).toEval(3);
    });

    it('Should break down compound math expressions as both operands', () => {
      let result = parseText('y = a * 2 + 3 * b;') as MinLogicInstructionList;

      expect(result).toBeTruthy();
      expect(Array.isArray(result)).toBeTruthy();
      expect(result.length).toEqual(3);

      // [a * 2]
      let result0 = result[0] as MinLogicOperation;
      expect(result0.type).toEqual('mul');
      expect(result0.outputVar).toBeTruthy();
      let tempVar0 = result0.outputVar;
      expect(result0.op1).toEval('a');
      expect(result0.op2).toEval(2);

      // [3 * b]
      let result1 = result[1] as MinLogicOperation;
      expect(result1.type).toEqual('mul');
      expect(result1.outputVar).toBeTruthy();
      let tempVar1 = result1.outputVar;
      expect(result1.op1).toEval(3);
      expect(result1.op2).toEval('b');

      // [~t1 + ~t2]
      let resultf = result[2] as MinLogicOperation;
      expect(resultf.type).toEqual('add');
      expect(resultf.outputVar).toEval('y');
      expect(resultf.op1).toBe(tempVar0);
      expect(resultf.op2).toBe(tempVar1);
    });

    it('should handle parentheses', () => {
      let result = parseText('index = (1);') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicSet);

      let result0 = result[0] as MinLogicSet;
      expect(result0.outputVar).toEval('index');
      expect(result0.setValue).toEval(1);
    });

    it('should handle parentheses order of operations', () => {
      let result = parseText('index = 3 * (1 + 2);') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);
      expect(result[0] instanceof MinLogicOperation);
      expect(result[1] instanceof MinLogicOperation);

      // [~t1 = 1 + 2]
      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEqual('add');
      expect(instr.op1).toEval(1);
      expect(instr.op2).toEval(2);

      // [index = 3 * ~t1]
      instr = result[1] as MinLogicOperation;
      expect(instr.outputVar).toEval('index');
      expect(instr.type).toEqual('mul');
      expect(instr.op1).toEval(3);
      expect(instr.op2).toBe(tempVar1);
    });

    it('should handle math functions', () => {
      let result = parseText(
        'index = 3 * cos(2); y = max(0, b);'
      ) as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(3);
      expect(result[0] instanceof MinLogicOperation);
      expect(result[1] instanceof MinLogicOperation);
      expect(result[2] instanceof MinLogicOperation);

      // [~t1 = 1 + 2]
      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEqual('cos');
      expect(instr.op1).toEval(2);

      // [index = 3 * ~t1]
      instr = result[1] as MinLogicOperation;
      expect(instr.outputVar).toEval('index');
      expect(instr.type).toEqual('mul');
      expect(instr.op1).toEval(3);
      expect(instr.op2).toBe(tempVar1);

      // y = atan2(5)
      instr = result[2] as MinLogicOperation;
      expect(instr.outputVar).toEval('y');
      expect(instr.type).toEqual('max');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toEval('b');
    });
  });

  describe('Boolean Logic', () => {
    it('should handle equality comparison', () => {
      let result = parseText('bool = 0 == 1;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicOperation);

      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toEval('bool');
      expect(instr.type).toEval('equal');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toEval(1);
    });

    it('should handle boolean and', () => {
      let result = parseText('bool = 0 && 1;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicOperation);

      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toEval('bool');
      expect(instr.type).toEval('land');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toEval(1);
    });

    it('should handle boolean or', () => {
      let result = parseText('bool = 0 || 1;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicOperation);

      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toEval('bool');
      expect(instr.type).toEval('or');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toEval(1);
    });

    it('should handle negation', () => {
      let result = parseText('bool = !0;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicOperation);

      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toEval('bool');
      expect(instr.type).toEval('xor');
      expect(instr.op1).toEval(0);
      expect(instr.op2).toEval(1);
    });

    it('should handle relational operators', () => {
      let result = parseText('bool = -5 >= 4;') as Array<any>;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);
      expect(result[0] instanceof MinLogicOperation);

      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toEval('bool');
      expect(instr.type).toEval('greaterThanEq');
      expect(instr.op1).toEval(-5);
      expect(instr.op2).toEval(4);
    });
  });

  describe('Full Compile', () => {
    it('should compile a small sequential program of operations and assignments', () => {
      let text = `bool = 1; var = 5 - bool * 10; result = var < 10 && var > 5;`;
      let result = parseText(text) as MinLogicInstructionList;
      let expected = `  set bool 1
                        op mul ~t1 bool 10
                        op sub var 5 ~t1
                        op lessThan ~t3 var 10
                        op greaterThan ~t4 var 5
                        op land result ~t3 ~t4
                        `.split('\n');

      let output = result.compile().split('\n');

      for (let i = 0; i < expected.length; i++)
        expect(expected[i].trim()).toEqual(
          output[i].trim(),
          `Line ${i} doesn't match:\n${expected[i]}\n${output[i]}`
        );
    });

    it('should compile a small while loop', () => {
      let text = `while (i < 10) { i = i + 1; } print(i); printflush(message1);`;
      let result = parseText(text) as MinLogicInstructionList;
      let expected = `  op lessThan ~t1 i 10
                        jump 4 notEqual ~t1 1
                        op add i i 1
                        jump 0 always 1 1
                        print i
                        printflush message1
                        `.split('\n');

      let output = result.compile().split('\n');

      for (let i = 0; i < expected.length; i++)
        expect(expected[i].trim()).toEqual(
          output[i].trim(),
          `Line ${i} doesn't match:\n${expected[i]}\n${output[i]}`
        );
    });
  });

  describe('Branches', () => {
    it('should loop while', () => {
      let text = `while (i < 10) {i = i + 1;}`;
      let result = parseText(text) as MinLogicInstructionList;
      result.computeLineNumbers();

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(4);
      expect(result[0] instanceof MinLogicOperation);
      expect(result[1] instanceof MinLogicJump);
      expect(result[2] instanceof MinLogicOperation);
      expect(result[3] instanceof MinLogicJump);

      // i < 10
      let instr = result[0] as MinLogicOperation;
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEval('lessThan');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(10);

      // Jump bottom if condition false
      let jump = result[1] as MinLogicJump;
      expect(jump.target.lineNumber).toEqual(3);
      expect(jump.type).toEval('notEqual');
      expect(jump.op1).toBe(tempVar1);
      expect(jump.op2).toEval(1);

      // i = i + 1
      instr = result[2] as MinLogicOperation;
      expect(instr.outputVar).toEval('i');
      expect(instr.type).toEval('add');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(1);

      // jump back always
      jump = result[3] as MinLogicJump;
      expect(jump.target.lineNumber).toEqual(0);
      expect(jump.type).toEval('always');
    });

    it('should branch a single condition', () => {
      let text = `if (i < 10) { i = i * 2; }`;
      let result = parseText(text) as MinLogicInstructionList;
      result.computeLineNumbers();

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(3);

      // 0: 1st conditional expression
      //    i < 10
      let instr = result[0] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEval('lessThan');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(10);

      // 1: Jump to end if condition false
      let jump = result[1] as MinLogicJump;
      expect(jump instanceof MinLogicJump).toBeTruthy();
      expect(jump.target.lineNumber).toEqual(2);
      expect(jump.type).toEval('notEqual');
      expect(jump.op1).toBe(tempVar1);
      expect(jump.op2).toEval(1);

      // 2: First conditional block
      //    i = i * 2
      instr = result[2] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toEval('i');
      expect(instr.type).toEval('mul');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(2);
    });

    it('should branch conditionally multiple if and else', () => {
      let text = `if (i < 10) { i = i * 2; } 
                  else if (i >= 50) { i = i - 10; }
                  else { i = i + 5; }`;
      let result = parseText(text) as MinLogicInstructionList;
      result.computeLineNumbers();

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(9);

      // 0: 1st conditional expression
      //    i < 10
      let instr = result[0] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEval('lessThan');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(10);

      // 1: Jump to 2nd block if condition false
      let jump = result[1] as MinLogicJump;
      expect(jump instanceof MinLogicJump).toBeTruthy();
      expect(jump.target.lineNumber).toEqual(3);
      expect(jump.type).toEval('notEqual');
      expect(jump.op1).toBe(tempVar1);
      expect(jump.op2).toEval(1);

      // 2: First conditional block
      //    i = i * 2
      instr = result[2] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toEval('i');
      expect(instr.type).toEval('mul');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(2);

      // 3: End of 1st conditional block
      //    Jump to end of if block
      jump = result[3] as MinLogicJump;
      expect(jump instanceof MinLogicJump).toBeTruthy();
      expect(jump.target.lineNumber).toEqual(8);
      expect(jump.type).toEval('always');

      // 4: 2nd conditional expression
      //    i > 50
      instr = result[4] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toBeDefined();
      let tempVar2 = instr.outputVar;
      expect(instr.type).toEval('greaterThanEq');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(50);

      // 5: Jump to else block if condition false
      jump = result[5] as MinLogicJump;
      expect(jump instanceof MinLogicJump).toBeTruthy();
      expect(jump.target.lineNumber).toEqual(7);
      expect(jump.type).toEval('notEqual');
      expect(jump.op1).toBe(tempVar2);
      expect(jump.op2).toEval(1);

      // 6: Second conditional block
      //    i = i - 10
      instr = result[6] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toEval('i');
      expect(instr.type).toEval('sub');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(10);

      // 7: End of 2nd conditional block
      //    Jump to end of if block
      jump = result[7] as MinLogicJump;
      expect(jump instanceof MinLogicJump).toBeTruthy();
      expect(jump.target.lineNumber).toEqual(8);
      expect(jump.type).toEval('always');

      // 8: Else Block
      //    i = i + 5
      instr = result[8] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toEval('i');
      expect(instr.type).toEval('add');
      expect(instr.op1).toEval('i');
      expect(instr.op2).toEval(5);
    });
  });

  describe('Instructions', () => {
    it('should read from cell', () => {
      let text = `read(var1, cell1, 0); read(var1, cell1, id);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);

      let print = result[0] as MinLogicRead;
      expect(print instanceof MinLogicRead).toBeTruthy();
      expect(print.varName).toEval('var1');
      expect(print.cellName).toEval('cell1');
      expect(print.index).toEval(0);

      print = result[1] as MinLogicRead;
      expect(print instanceof MinLogicRead).toBeTruthy();
      expect(print.index).toEval('id');
    });

    it('should write to a cell', () => {
      let text = `write(var1, cell1, 0); write(var1, cell1, id);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);

      let print = result[0] as MinLogicWrite;
      expect(print instanceof MinLogicWrite).toBeTruthy();
      expect(print.varName).toEval('var1');
      expect(print.cellName).toEval('cell1');
      expect(print.index).toEval(0);

      print = result[1] as MinLogicWrite;
      expect(print instanceof MinLogicWrite).toBeTruthy();
      expect(print.index).toEval('id');
    });

    it('should draw', () => {
      let text = `draw(triangle, x, y, x2, y2, x3, y3);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicDraw;
      expect(print instanceof MinLogicDraw).toBeTruthy();
      expect(print.compile()).toEqual('draw triangle x y x2 y2 x3 y3');
    });

    it('should drawflush', () => {
      let text = `drawflush(display1);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicDrawFlush;
      expect(print instanceof MinLogicDrawFlush).toBeTruthy();
      expect(print.displayName).toEval('display1');
    });

    it('should print', () => {
      let text = `print("hello world");`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what).toEval('"hello world"');
    });

    it('should printflush', () => {
      let text = `printflush(message1);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicPrintFlush;
      expect(print instanceof MinLogicPrintFlush).toBeTruthy();
      expect(print.displayName).toEval('message1');
    });

    it('should printflush and ignore second argument', () => {
      let text = `printflush(message1, message2);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicPrintFlush;
      expect(print instanceof MinLogicPrintFlush).toBeTruthy();
      expect(print.displayName).toEval('message1');
    });

    it('should print multiple expressions', () => {
      let text = `print("hello world ", name, "\\n", 1+3, @time);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(6);

      let instr = result[0] as MinLogicOperation;
      expect(instr instanceof MinLogicOperation).toBeTruthy();
      expect(instr.outputVar).toBeDefined();
      let tempVar1 = instr.outputVar;
      expect(instr.type).toEval('add');

      let print = result[1] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what).toEval('"hello world "');

      print = result[2] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what instanceof MinLogicReference).toBeTruthy();
      expect(print.what).toEval('name');

      print = result[3] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what).toEval('"\\n"');

      print = result[4] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what).toEval(tempVar1);

      print = result[5] as MinLogicPrint;
      expect(print instanceof MinLogicPrint).toBeTruthy();
      expect(print.what).toEval('@time');
    });

    it('should link', () => {
      let text = `getlink(unit1);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let print = result[0] as MinLogicLink;
      expect(print instanceof MinLogicLink).toBeTruthy();
      expect(print.varName).toEval('unit1');
    });

    it('should control', () => {
      let text = `control(shoot, duo1, sx, sy, true);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let controlInstr = result[0] as MinLogicControl;
      expect(controlInstr instanceof MinLogicControl).toBeTruthy();
      expect(controlInstr.command).toEval('shoot');
      expect(controlInstr.compile()).toEqual('control shoot duo1 sx sy true');
    });

    it('should radar', () => {
      let text = `radar(enemy, ground, boss, distance, duo1, 0, result);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let radarInstr = result[0] as MinLogicRadar;
      expect(radarInstr instanceof MinLogicRadar).toBeTruthy();
      expect(radarInstr.compile()).toEqual(
        'radar enemy ground boss distance duo1 0 result'
      );
    });

    it('should sensor', () => {
      let text = `sensor(result, block1, @copper);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let sensorInstr = result[0] as MinLogicSensor;
      expect(sensorInstr instanceof MinLogicSensor).toBeTruthy();
      expect(sensorInstr.outputVar).toEval('result');
      expect(sensorInstr.type).toEval('block1');
      expect(sensorInstr.block).toEval('@copper');
      expect(sensorInstr.compile()).toEqual('sensor result block1 @copper');
    });

    it('should end', () => {
      let text = `end();`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let endInstr = result[0] as MinLogicEnd;
      expect(endInstr instanceof MinLogicEnd).toBeTruthy();
      expect(endInstr.compile()).toEqual('end');
    });

    it('should unit bind', () => {
      let text = `ubind(@poly);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let bindInstr = result[0] as MinLogicUnitBind;
      expect(bindInstr instanceof MinLogicUnitBind).toBeTruthy();
      expect(bindInstr.type).toEval('@poly');
      expect(bindInstr.compile()).toEqual('ubind @poly');
    });

    it('should unit control', () => {
      let text = `ucontrol(approach, x, y, r);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let bindInstr = result[0] as MinLogicUnitControl;
      expect(bindInstr instanceof MinLogicUnitControl).toBeTruthy();
      expect(bindInstr.compile()).toEqual('ucontrol approach x y r');
    });

    it('should unit radar', () => {
      let text = `uradar(enemy, ground, boss, distance, order, result);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let radarInstr = result[0] as MinLogicUnitRadar;
      expect(radarInstr instanceof MinLogicUnitRadar).toBeTruthy();
      expect(radarInstr.compile()).toEqual(
        'uradar enemy ground boss distance 0 order result'
      );
    });

    it('should unit locate', () => {
      let text = `ulocate(building, core, true, outx, outy, found, result);`;
      let result = parseText(text) as MinLogicInstructionList;

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(1);

      let radarInstr = result[0] as MinLogicUnitLocate;
      expect(radarInstr instanceof MinLogicUnitLocate).toBeTruthy();
      expect(radarInstr.compile()).toEqual(
        'ulocate building core true 0 outx outy found result'
      );
    });
  });

  describe('Inline functions', () => {
    it('should parse the symbol tree', () => {
      let text = `
        print "let's go!"
        inline add(a, b) {
            result = a + b; # temp var: ~add-result-1
            a = a + 1;
            return a + b;
        }
        
        inline sub(a, b) {
            return a - b;
        }

        a = 1;
        b = 2;
        c = 3;
        
        result = add(b, c);
      `;
      let inputStream = CharStreams.fromString(text);
      let lexer = new MinLangLexer(inputStream);
      let tokenStream = new CommonTokenStream(lexer);
      let parser = new MinLangParser(tokenStream);
      let tree = parser.parse();
      let exe = new MinLangSymbolVisitor();
      let result = exe.visit(tree);
      expect(result).toBeDefined();

      console.log(result);
    });

    it('should add an inline function with math expression as an argument', () => {
      let text = `
            inline add(a, b) {
                return a * b;
            }
            
            v = 42;
            result = add(v, 1 + 2);
          `;
      let result = parseText(text) as MinLogicInstructionList;
      result.expandInlineFunctions();
      console.log('final:\n', result.compile());

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(3);

      let setInstr = result[0] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('v');
      expect(setInstr.setValue).toEval(42);

      let opInstr = result[1] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toBeDefined();
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval(1);
      expect(opInstr.op2).toEval(2);
      let tempVar1 = opInstr.outputVar;

      opInstr = result[2] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.type).toEval('mul');
      expect(opInstr.op1).toEval('v');
      expect(opInstr.op2).toBe(tempVar1);

      //   print = result[1] as MinLogicRead;
      //   expect(print instanceof MinLogicRead).toBeTruthy();
      //   expect(print.index).toEval('id');
    });

    it('should add a complicated inline function', () => {
      let text = `
        inline add(a, b) {
            result = a + b; # temp var: ~add-result-1
            a = a + 1;
            c = 99;
            return a + b;
        }
        
        a = 1;
        b = 2;
        c = 3;
        
        result = add(b, 1 + 1);
      `;
      let result = parseText(text) as MinLogicInstructionList;
      result.expandInlineFunctions();
      console.log('final:\n', result.compile());

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(8);

      let setInstr = result[0] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('a');
      expect(setInstr.setValue).toEval(1);

      setInstr = result[1] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('b');
      expect(setInstr.setValue).toEval(2);

      setInstr = result[2] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('c');
      expect(setInstr.setValue).toEval(3);

      let opInstr = result[3] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toBeDefined();
      let tempVar1 = opInstr.outputVar;
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval(1);
      expect(opInstr.op2).toEval(1);

      opInstr = result[4] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('~add~result');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval('b');
      expect(opInstr.op2).toBe(tempVar1);

      opInstr = result[5] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('~add~a');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval('b');
      expect(opInstr.op2).toEval(1);

      setInstr = result[6] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('~add~c');
      expect(setInstr.setValue).toEval(99);

      opInstr = result[7] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('result');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval('b');
      expect(opInstr.op2).toBe(tempVar1);
    });

    it('should add two of the same inline functions', () => {
      let text = `
        inline add(a, b) {
            return a + b;
        }
        
        result = add(1, 2);
        var = add(3, c);
        #result = add(add(11, 12) * 3, 13);
      `;
      let result = parseText(text) as MinLogicInstructionList;
      result.expandInlineFunctions();
      console.log('final:\n', result.compile());

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);

      let opInstr = result[0] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('result');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval(1);
      expect(opInstr.op2).toEval(2);

      opInstr = result[1] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('var');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval(3);
      expect(opInstr.op2).toEval('c');
    });

    it('should handle nested inline functions', () => {
      let text = `
        inline add(a, b) {
            return a + b;
        }
        
        result = add(add(11, 12) * 3, 13);
      `;
      let result = parseText(text) as MinLogicInstructionList;
      result.expandInlineFunctions();
      console.log('final:\n', result.compile());

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(3);

      let opInstr = result[0] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toBeDefined();
      let tempVar1 = opInstr.outputVar;
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toEval(11);
      expect(opInstr.op2).toEval(12);

      opInstr = result[1] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toBeDefined();
      let tempVar2 = opInstr.outputVar;
      expect(opInstr.type).toEval('mul');
      expect(opInstr.op1).toBe(tempVar1);
      expect(opInstr.op2).toEval('3');

      opInstr = result[2] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).toEval('result');
      expect(opInstr.type).toEval('add');
      expect(opInstr.op1).toBe(tempVar2);
      expect(opInstr.op2).toEval('13');
    });

    it('should handle no return clause', () => {
      let text = `
        inline increment(a) {
            a = a + 1;
        }
        
        a = 1
        increment(a);
      `;
      let result = parseText(text) as MinLogicInstructionList;
      result.expandInlineFunctions();
      console.log('final:\n', result.compile());

      expect(result).toBeDefined();
      expect(result instanceof MinLogicInstructionList).toBeTruthy();
      expect(result.length).toEqual(2);

      let setInstr = result[0] as MinLogicSet;
      expect(setInstr instanceof MinLogicSet).toBeTruthy();
      expect(setInstr.outputVar).toEval('a');
      expect(setInstr.setValue).toEval(1);

      let opInstr = result[1] as MinLogicOperation;
      expect(opInstr instanceof MinLogicOperation).toBeTruthy();
      expect(opInstr.outputVar).not.toEval('a');
      expect(opInstr.op1).toEval('a');
      expect(opInstr.op2).toEval(1);
    });
  });
});

function parseText(inputText: string): MinLogicInstructionList {
  let inputStream = CharStreams.fromString(inputText);
  let lexer = new MinLangLexer(inputStream);
  let tokenStream = new CommonTokenStream(lexer);
  let parser = new MinLangParser(tokenStream);
  let tree = parser.parse();
  let exe = new MinLangExecutor();
  let result = exe.visit(tree) as MinLogicInstructionList;
  return result;
}
