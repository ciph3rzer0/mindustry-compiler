import { Interval } from 'antlr4ts/misc/Interval';
import { AbstractParseTreeVisitor } from 'antlr4ts/tree/AbstractParseTreeVisitor';
import { RuleNode } from 'antlr4ts/tree/RuleNode';
import {
  AdditiveExprContext,
  AndExprContext,
  AssignmentContext,
  AtomContext,
  AtomExprContext,
  BlockContext,
  BooleanAtomContext,
  Condition_blockContext,
  ControlInstructionContext,
  Declare_inline_functionContext,
  DrawFlushInstructionContext,
  DrawInstructionContext,
  EndInstructionContext,
  EqualityExprContext,
  ExprContext,
  FunctionExprContext,
  Function_exprContext,
  IdAtomContext,
  If_statContext,
  LinkInstructionContext,
  MathFuncExprContext,
  MinLangParser,
  MultiplicationExprContext,
  Native_instructionContext,
  NotExprContext,
  NullAtomContext,
  NumberAtomContext,
  OrExprContext,
  ParExprContext,
  ParseContext,
  PowExprContext,
  PrintFlushInstructionContext,
  PrintInstructionContext,
  RadarInstructionContext,
  ReadInstructionContext,
  RelationalExprContext,
  Return_statContext,
  SensorInstructionContext,
  StatContext,
  Stat_blockContext,
  StringAtomContext,
  UnaryMinusExprContext,
  UnitBindInstructionContext,
  UnitControlInstructionContext,
  UnitLocateInstructionContext,
  UnitRadarInstructionContext,
  While_statContext,
  WriteInstructionContext,
} from './c/MinLangParser';
import { MinLangVisitor } from './c/MinLangVisitor';
import {
  MinLogicBool,
  MinLogicControl,
  MinLogicDeclareInline,
  MinLogicDraw,
  MinLogicDrawFlush,
  MinLogicEnd,
  MinLogicInlineExpression,
  MinLogicInstructionList,
  MinLogicJump,
  MinLogicLink,
  MinLogicLiteral,
  MinLogicNativeInstruction,
  MinLogicNumber,
  MinLogicOperation,
  MinLogicPrint,
  MinLogicPrintFlush,
  MinLogicRadar,
  MinLogicRead,
  MinLogicReference,
  MinLogicSensor,
  MinLogicSet,
  MinLogicString,
  MinLogicUnitBind,
  MinLogicUnitControl,
  MinLogicUnitLocate,
  MinLogicUnitRadar,
  MinLogicWrite,
} from './MinLogic';

// export type Atom = string | number | boolean | void;
export type Value = MinLogicInstructionList | MinLogicLiteral;

export class MinLangExecutor
  extends AbstractParseTreeVisitor<Value>
  implements MinLangVisitor<Value> {
  protected defaultResult(): Value {
    return;
    // throw new Error('Method not implemented.');
  }

  tempVarNum: number = 0;
  getTempVar(): MinLogicReference {
    this.tempVarNum++;
    return new MinLogicReference('~t' + this.tempVarNum);
  }
  /**
   * Atoms
   */
  visitAtomExpr(ctx: AtomExprContext) {
    // console.log(`--- AtomExpr: ${ctx.text}, children: ${ctx.childCount}`);
    var result = ctx.atom().accept(this);
    // console.log(`--- AtomExpr.result: ${result}`);
    return result;
  }
  visitAtom(ctx: AtomContext) {
    throw 'atom not implemented';
    return null;
    console.log(`--- Atom: ${ctx.text}, children: ${ctx.childCount}`);
  }
  visitBooleanAtom(ctx: BooleanAtomContext) {
    console.log(`boolean:[${ctx.text}], children: ${ctx.childCount}`);
    return new MinLogicBool(ctx.text);
  }
  visitNumberAtom(ctx: NumberAtomContext) {
    // console.log(`number:[${ctx.text}]`);
    return new MinLogicNumber(Number(ctx.text));
  }
  visitStringAtom(ctx: StringAtomContext) {
    console.log(`string:[${ctx.text}], children: ${ctx.childCount}`);
    return new MinLogicString(ctx.text);
  }
  visitNilAtom(ctx: NullAtomContext) {
    console.log(`null:[${ctx.text}]`);
  }
  visitIdAtom(ctx: IdAtomContext) {
    // console.log(`identifier:[${ctx.text}], children: ${ctx.childCount}`);
    return new MinLogicReference(ctx.text);
  }

  /**
   * Top Level Structure
   */
  // Entry point
  // @Override
  visitParse(ctx: ParseContext) {
    console.log(`--- Parse: ${ctx.text}, children: ${ctx.childCount}`);
    let result = ctx.block().accept(this);
    console.log(`--- Parse.result: ${result}`);

    return result;
  }

  // Main code blocks
  visitBlock(ctx: BlockContext) {
    console.log(`- Block: ${ctx.text}, children: ${ctx.childCount}`);

    var statements = ctx.stat();
    // console.log(`block statements: ${statements}`);

    let codeBlocks = new MinLogicInstructionList();

    statements.forEach((s) => {
      var result = s.accept(this);
      codeBlocks.merge(result);
    });
    console.log(`--- Block.result: ${codeBlocks}`);

    return codeBlocks;
  }

  // Statement
  visitStat(ctx: StatContext) {
    console.log(`--- Statement: ${ctx.text}, children: ${ctx.childCount}`);
    let result = ctx.getChild(0).accept(this) as any;
    // console.log(`--- Statement >>>>>>>>>>>>> : ${result.constructor.name}`);
    // console.log(`--- Statement >>>>>>>>>>>>> : ${result[0].constructor.name}`);
    // console.log(
    //   `--- Statement >>>>>>>>>>>>> : ${result[0].setValue.constructor.name}`
    // );
    // console.log(`--- Statement.result: ${result}`);
    return result;
  }

  visitWhile_stat(ctx: While_statContext) {
    console.log(`WhileStat ${ctx.expr().text}`);
    let { parsedArgs, list } = this.parseArgs([ctx.expr()]);

    let one = new MinLogicNumber(1);
    let jumpBack = new MinLogicJump(list.last(), 'always', one, one, 0);
    let jumpOver = new MinLogicJump(jumpBack, 'notEqual', parsedArgs[0], one);

    list.merge(jumpOver);
    let innerCode = ctx.stat_block().accept(this);
    console.log(innerCode);
    list.merge(innerCode);
    list.merge(jumpBack);

    return list;
  }

  visitIf_stat(ctx: If_statContext) {
    console.log(`IfStat ${ctx.text}`);
    var list = new MinLogicInstructionList();
    const one = new MinLogicNumber(1);

    // Update references later to jump commands exiting the conditional block
    let endJumps = [];

    // We will skip the last jump to save an instruction
    let lastBlock = ctx.stat_block()
      ? ctx.stat_block()
      : ctx.condition_block(ctx.condition_block().length - 1);

    ctx.condition_block().forEach((block) => {
      let { parsedArgs, list: conditionList } = this.parseArgs([block.expr()]);
      let condition = parsedArgs[0];

      // The commands inside the conditional block
      let blockList = block
        .stat_block()
        .accept(this) as MinLogicInstructionList;

      let jumpToNextBlock = new MinLogicJump(
        blockList.last(),
        'notEqual',
        condition,
        one
      );

      list.merge(conditionList);
      list.merge(jumpToNextBlock);
      list.merge(blockList);

      if (block != lastBlock) {
        let jumpToEnd = new MinLogicJump(null, 'always', one, one);
        endJumps.push(jumpToEnd);
        list.merge(jumpToEnd);

        jumpToNextBlock.target = jumpToEnd;
      }
    });

    // Else block
    if (ctx.stat_block()) {
      let elseBlock = ctx.stat_block().accept(this) as MinLogicInstructionList;
      list.merge(elseBlock);
    }

    // Set all conditional blocks to jump to last when done
    endJumps.forEach((jump) => {
      jump.target = list.last();
    });

    return list;
  }

  visitDeclare_inline_function(ctx: Declare_inline_functionContext) {
    let id = ctx.ID().text;
    let params = ctx
      .parameters()
      .ID()
      .map((p) => p.text);

    console.log(`DeclareInline ${id} ${JSON.stringify(params)}`);

    let inline_block = ctx.stat_block().accept(this);
    let returnVar = null;

    if (ctx.stat_block().block().return_stat()) {
      let returnExpr = ctx.stat_block().block().return_stat().accept(this);
      let lastOp = returnExpr.last() as MinLogicOperation;
      if (!(lastOp instanceof MinLogicOperation))
        throw 'exception in visitDeclare_inline_function';

      inline_block.merge(returnExpr);
      returnVar = lastOp.outputVar;
    }
    return new MinLogicDeclareInline(id, params, inline_block, returnVar);
  }

  visitFunction_expr(ctx: Function_exprContext) {
    console.log(`Func_Expr ${ctx.ID().text} (${ctx.arguments().text})`);
    let id = ctx.ID().text;
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());
    console.log(`Func_Expr ${ctx.ID().text} (${parsedArgs.join(', ')})`);
    console.log(`Func_Expr.list ${list}`);

    return list.merge(
      new MinLogicInlineExpression(this.getTempVar(), id, parsedArgs)
    );
  }

  // The actual expression?
  visitFunctionExpr(ctx: FunctionExprContext) {
    console.log(`FuncExpr ${ctx.text}`);
    return ctx.function_expr().accept(this);
  }

  visitReturn_stat(ctx: Return_statContext) {
    console.log(`ReturnStat ${ctx.expr().text}`);
    return ctx.expr().accept(this);
    // let { parsedArgs, list } = this.parseArgs([ctx.expr()]);
    //   console.log(`ReturnStat ${parsedArgs[0]}`);
  }

  visitCondition_block(ctx: Condition_blockContext) {
    throw 'conditional block not implemented';
    return null;
    console.log('--- Conditional Block: ', ctx.text);
  }

  visitStat_block(ctx: Stat_blockContext) {
    if (ctx.block()) {
      console.log(`StatBlock ${ctx.block().text}`);
      return ctx.block().accept(this);
    } else if (ctx.stat()) {
      console.log(`StatBlock ${ctx.stat().text}`);
      return ctx.stat().accept(this);
    } else {
      throw 'unknown stat block ${ctx}';
    }
  }

  //   visitInstruction(ctx: InstructionContext) {
  //     console.log(
  //       `Instruction ${ctx.command().text} args: ${ctx.arguments().text}`
  //     );
  //   }

  parseArgs(args: ExprContext[]) {
    var list = new MinLogicInstructionList();
    var parsedArgs: Array<MinLogicLiteral> = [];

    args.forEach((a) => {
      let result = a.accept(this);
      if (result instanceof MinLogicLiteral) {
        parsedArgs.push(result);
      } else if (result instanceof MinLogicInstructionList) {
        let lastOp = result.last();
        if (
          lastOp instanceof MinLogicOperation ||
          lastOp instanceof MinLogicInlineExpression
        ) {
          list.merge(result);
          parsedArgs.push(lastOp.outputVar);
        } else {
          throw `unknown result in while parsing args`;
        }
      }
    });

    return { list, parsedArgs };
  }

  visitNative_instruction(ctx: Native_instructionContext) {
    let a = ctx.start.startIndex;
    let b = ctx.stop.stopIndex;
    let interval = new Interval(a, b);
    let text = ctx.start.inputStream.getText(interval);

    console.log(`MinInstr, args: ${text}`);
    return new MinLogicNativeInstruction(text).toList();
  }

  visitReadInstruction(ctx: ReadInstructionContext) {
    console.log(`ReadInstr, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let varName = parsedArgs[0] as MinLogicReference;
    let cellName = parsedArgs[1];
    let index = parsedArgs[2];

    list.merge(new MinLogicRead(varName, cellName, index));
    return list;
  }

  visitWriteInstruction(ctx: WriteInstructionContext) {
    console.log(`WriteInstr, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let varName = parsedArgs[0] as MinLogicReference;
    let cellName = parsedArgs[1];
    let index = parsedArgs[2];

    list.merge(new MinLogicWrite(varName, cellName, index));
    return list;
  }

  visitPrintInstruction(ctx: PrintInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    parsedArgs.forEach((a) => {
      //@ts-ignore
      list.merge(new MinLogicPrint(a));
    });

    return list;
  }

  visitPrintFlushInstruction(ctx: PrintFlushInstructionContext) {
    console.log(`PrintFlush, args: ${ctx.arguments().text}`);
    var list = new MinLogicInstructionList();

    let displayName = ctx.arguments().expr()[0].text;
    list.merge(new MinLogicPrintFlush(displayName));

    return list;
  }

  visitDrawInstruction(ctx: DrawInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let command = parsedArgs.shift();
    let args = parsedArgs.map((e) => e.value + '');

    list.merge(new MinLogicDraw(command, args));

    return list;
  }

  visitDrawFlushInstruction(ctx: DrawFlushInstructionContext) {
    console.log(`PrintFlush, args: ${ctx.arguments().text}`);
    var list = new MinLogicInstructionList();

    let displayName = ctx.arguments().expr()[0].text;
    list.merge(new MinLogicDrawFlush(displayName));

    return list;
  }

  visitLinkInstruction(ctx: LinkInstructionContext) {
    console.log(`LinkInstr, args: ${ctx.arguments().expr(0).text}`);
    return new MinLogicLink(
      new MinLogicReference(ctx.arguments().expr(0).text)
    ).toList();
  }

  visitControlInstruction(ctx: ControlInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let command = parsedArgs.shift();
    let args = parsedArgs.map((e) => e.value + '');

    list.merge(new MinLogicControl(command, args));

    return list;
  }

  visitRadarInstruction(ctx: RadarInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let args = parsedArgs.map((e) => e.value + '');

    list.merge(
      new MinLogicRadar(
        args[0],
        args[1],
        args[2],
        parsedArgs[3] as MinLogicNumber,
        parsedArgs[4] as MinLogicReference,
        parsedArgs[5] as MinLogicNumber,
        parsedArgs[6] as MinLogicReference
      )
    );

    return list;
  }

  visitSensorInstruction(ctx: SensorInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let outputVar = parsedArgs[0] as MinLogicReference;
    let type = parsedArgs[1] as MinLogicReference;
    let block = parsedArgs[2] as MinLogicReference;

    list.merge(new MinLogicSensor(outputVar, type, block));

    return list;
  }

  visitEndInstruction(_ctx: EndInstructionContext) {
    return new MinLogicEnd().toList();
  }

  visitUnitBindInstruction(ctx: UnitBindInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    return new MinLogicUnitBind(ctx.arguments().expr(0).text).toList();
  }

  visitUnitControlInstruction(ctx: UnitControlInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let command = parsedArgs.shift();
    let args = parsedArgs.map((e) => e.value + '');

    list.merge(new MinLogicUnitControl(command, args));

    return list;
  }

  visitUnitRadarInstruction(ctx: UnitRadarInstructionContext) {
    console.log(`Instruction, args: ${ctx.arguments().text}`);
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());

    let args = parsedArgs.map((e) => e.value + '');

    list.merge(
      new MinLogicUnitRadar(
        args[0],
        args[1],
        args[2],
        parsedArgs[3] as MinLogicNumber,
        args[4] as string,
        parsedArgs[5] as MinLogicReference
      )
    );

    return list;
  }

  visitUnitLocateInstruction(ctx: UnitLocateInstructionContext) {
    let { list, parsedArgs } = this.parseArgs(ctx.arguments().expr());
    let args = parsedArgs.map((e) => e.value + '');

    list.merge(
      new MinLogicUnitLocate(
        args[0],
        args[1],
        args[2],
        parsedArgs[3] as MinLogicReference,
        parsedArgs[4] as MinLogicReference,
        args[5],
        parsedArgs[6] as MinLogicReference
      )
    );

    return list;
  }

  // ---------------

  visitExpr(ctx: ExprContext) {
    console.log('--- Expr Context: ', ctx.text);
    throw 'visitExpr not implemented';
    return null;
  }
  visitParExpr(ctx: ParExprContext) {
    console.log('Paren: ', ctx.text);
    return ctx.expr().accept(this);
  }

  /**
   * Math
   */

  visitAdditiveExpr(ctx: AdditiveExprContext) {
    console.log(`AddExpr ${ctx.expr(0).text} + ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    let type;
    switch (ctx._op.type) {
      case MinLangParser.PLUS:
        type = 'add';
        break;
      case MinLangParser.MINUS:
        type = 'sub';
        break;
      default:
        throw `invalid add op: ${ctx._op.type};`;
    }

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        type,
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitMultiplicationExpr(ctx: MultiplicationExprContext) {
    console.log(`MulExpr ${ctx.expr(0).text} x ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    let type;
    switch (ctx._op.type) {
      case MinLangParser.MULT:
        type = 'mul';
        break;
      case MinLangParser.DIV:
        type = 'div';
        break;
      case MinLangParser.INTDIV:
        type = 'idiv';
        break;
      case MinLangParser.MOD:
        type = 'mod';
        break;
      default:
        throw `invalid mul op: ${ctx._op.type};`;
    }

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        type,
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitPowExpr(ctx: PowExprContext) {
    console.log(`PowExpr ${ctx.expr(0).text} ^ ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        'pow',
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitMathFuncExpr(ctx: MathFuncExprContext) {
    console.log(`MathFunc ${ctx.arguments().text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.arguments().expr());

    if (!parsedArgs[1]) {
      parsedArgs[1] = new MinLogicNumber(0);
    }

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        ctx.math_function().text,
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }
  /**
   *
   */

  visitAssignment(ctx: AssignmentContext) {
    console.log(`--- Assignment: ${ctx.text}, children: ${ctx.childCount}`);
    let { parsedArgs, list } = this.parseArgs([ctx.expr()]);
    console.log(`--- Assignment.result =  ${parsedArgs[0]}\nlist = ${list}`);

    if (list.length) {
      let lastOp = list.last();
      if (
        lastOp instanceof MinLogicOperation ||
        lastOp instanceof MinLogicInlineExpression
      ) {
        lastOp.outputVar = new MinLogicReference(ctx.ID().text);
        console.log(`--- Assignment.outputVar =  ${lastOp.outputVar}`);
        return list;
      }
    }
    return list.merge(
      new MinLogicSet(new MinLogicReference(ctx.ID().text), parsedArgs[0])
    );
  }

  visitUnaryMinusExpr(ctx: UnaryMinusExprContext) {
    console.log(`UnaryMinusExpr ${ctx.expr().text}`);
    let { parsedArgs, list } = this.parseArgs([ctx.expr()]);

    if (parsedArgs[0] instanceof MinLogicNumber) {
      return new MinLogicNumber(-parsedArgs[0].value);
    }

    let zero = new MinLogicNumber(0);
    return list.merge(
      new MinLogicOperation(this.getTempVar(), 'sub', zero, parsedArgs[0])
    );
  }

  /**
   * Boolean Math
   */
  visitAndExpr(ctx: AndExprContext) {
    console.log(`AndExpr ${ctx.expr(0).text} and ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        'land',
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitOrExpr(ctx: OrExprContext) {
    console.log(`OrExpr ${ctx.expr(0).text} and ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        'or',
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitEqualityExpr(ctx: EqualityExprContext) {
    console.log(`EqualityExpr ${ctx.expr(0).text} == ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    let type;
    switch (ctx._op.type) {
      case MinLangParser.EQ:
        type = 'equal';
        break;
      case MinLangParser.NEQ:
        type = 'notEqual';
        break;
      default:
        throw `invalid equality op: ${ctx._op.type};`;
    }

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        type,
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  visitNotExpr(ctx: NotExprContext) {
    console.log(`NotExpr ${ctx.expr().text}`);
    let { parsedArgs, list } = this.parseArgs([ctx.expr()]);

    let one = new MinLogicNumber(1);
    return list.merge(
      new MinLogicOperation(this.getTempVar(), 'xor', parsedArgs[0], one)
    );
  }

  visitRelationalExpr(ctx: RelationalExprContext) {
    console.log(`RelationalExpr ${ctx.expr(0).text} and ${ctx.expr(1).text}`);
    let { parsedArgs, list } = this.parseArgs(ctx.expr());

    let type;
    switch (ctx._op.type) {
      case MinLangParser.LT:
        type = 'lessThan';
        break;
      case MinLangParser.LTEQ:
        type = 'lessThanEq';
        break;
      case MinLangParser.GT:
        type = 'greaterThan';
        break;
      case MinLangParser.GTEQ:
        type = 'greaterThanEq';
        break;
      default:
        throw `invalid relational op: ${ctx._op.type};`;
    }

    return list.merge(
      new MinLogicOperation(
        this.getTempVar(),
        type,
        parsedArgs[0],
        parsedArgs[1]
      )
    );
  }

  //   visit(tree: ParseTree): void {
  //     throw new Error('Method not implemented.');
  //   }
  visitChildren(node: RuleNode) {
    console.log('node: ', node);
    throw new Error('Method not implemented.');
    return null;
  }
  //   visitTerminal(node: TerminalNode): void {
  //     throw new Error('Method not implemented.');
  //   }
  //   visitErrorNode(node: ErrorNode): void {
  //     throw new Error('Method not implemented.');
  //   }
}
