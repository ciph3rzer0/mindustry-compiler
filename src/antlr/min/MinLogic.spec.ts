import {
  MinLogicDeclare,
  MinLogicDeclareInline,
  MinLogicInstructionList,
  MinLogicNumber,
  MinLogicOperation,
  MinLogicPrint,
  MinLogicReference,
  MinLogicSet,
  MinLogicString,
} from './MinLogic';

describe('MinLogic to string', () => {
  it('should create a set command', () => {
    let prop = new MinLogicSet(
      new MinLogicReference('var'),
      new MinLogicNumber(55)
    );

    let output = prop.compile() as string;
    expect(output).toEqual('set var 55');
  });

  it('should create a operation command', () => {
    let prop = new MinLogicOperation(
      new MinLogicReference('var'),
      'mul',
      new MinLogicNumber(55),
      new MinLogicReference('myVar')
    );

    let output = prop.compile() as string;
    expect(output).toEqual('op mul var 55 myVar');
  });

  describe('List Merging', () => {
    let func1_declare, func2_declare: MinLogicDeclare;
    let func1_block, func2_block: MinLogicInstructionList;
    let list1, list2: MinLogicInstructionList;

    beforeAll(() => {
      func1_block = new MinLogicOperation(
        new MinLogicReference('var'),
        'mul',
        new MinLogicNumber(55),
        new MinLogicReference('myVar')
      ).toList();
      func1_declare = new MinLogicDeclareInline(
        'func1',
        ['a'],
        func1_block,
        new MinLogicReference('~t1')
      );

      func2_block = new MinLogicOperation(
        new MinLogicReference('index'),
        'add',
        new MinLogicReference('index'),
        new MinLogicNumber(1)
      ).toList();
      func2_declare = new MinLogicDeclareInline(
        'func2',
        [],
        func2_block,
        new MinLogicReference('~t3')
      );

      list1 = new MinLogicPrint(new MinLogicString('Run Function 1')).toList();
      list1.declare(func1_declare.id, func1_declare);

      list2 = new MinLogicPrint(new MinLogicString('Run Function 2')).toList();
      list1.declare(func2_declare.id, func2_declare);

      list1.merge(list2);
    });

    it('should merge lists', () => {
      let list1 = new MinLogicOperation(
        new MinLogicReference('var'),
        'mul',
        new MinLogicNumber(55),
        new MinLogicReference('myVar')
      ).toList();

      let list2 = new MinLogicPrint(
        new MinLogicString('Hello World!!!')
      ).toList();

      list1.merge(list2);
      expect(list1.length).toEqual(2);
      expect(list1[0] instanceof MinLogicOperation).toBeTruthy();
      expect(list1[1] instanceof MinLogicPrint).toBeTruthy();
      expect(list1.symbols.size).toEqual(0);
    });

    it('should merge symbols when merging lists', () => {
      expect(list1.symbols.size).toEqual(2);
      expect(list1.symbols.get('func1')).toBeTruthy();
      expect(list1.symbols.get('func2')).toBeTruthy();
    });

    it('should error when merging symbols that conflict', () => {
      let list3 = new MinLogicInstructionList();
      list3.declare(
        'func2',
        new MinLogicDeclareInline(
          'func2',
          [],
          func2_block,
          new MinLogicReference('~t5')
        )
      );
      expect(() => list3.merge(list1)).toThrow();
    });
  });
});
