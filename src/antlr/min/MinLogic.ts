import { cloneDeep } from 'lodash';

export class MinLogicLiteral {
  constructor(public value: any) {}
  getType() {
    return this.constructor.name;
  }
  toString() {
    return `(${this.getType()}: ${this.value})`;
  }
}

export class MinLogicNumber extends MinLogicLiteral {
  constructor(public value: number) {
    super(value);
  }
  getType() {
    return 'number';
  }
}

export class MinLogicBool extends MinLogicLiteral {
  constructor(public value: string) {
    super(value);
    if (!(value == 'true' || value == 'false')) {
      throw `invalid boolean [${value}]`;
    }
  }
  getType() {
    return 'bool';
  }
}

export class MinLogicNull extends MinLogicLiteral {
  constructor() {
    super('null');
  }
  getType() {
    return 'null';
  }
}

export class MinLogicReference extends MinLogicLiteral {
  constructor(public value: string) {
    super(value);
  }
  getType() {
    return 'ref';
  }
  id(): string {
    return this.value;
  }
}

export class MinLogicString extends MinLogicLiteral {
  constructor(public value: string) {
    super(value);
  }
  getType() {
    return 'string:';
  }
  string(): string {
    return this.value;
  }
}

/**
 * Instructions
 */
export class MinLogicStatement {
  public lineNumber;
  compile(): string {
    throw 'MinLogicStatement compile not implemented.';
  }
  toString() {
    let keys = Object.keys(this);
    let props: Array<string> = [];

    keys.forEach((k) => {
      props.push(`${k}: ${this[k]}`);
    });

    return `{${this.constructor.name}: ${props.join(', ')}}`;
  }
  toList() {
    return new MinLogicInstructionList(this);
  }
}

export class MinLogicNativeInstruction extends MinLogicStatement {
  constructor(public source: string) {
    super();
  }
  compile(): string {
    return this.source;
  }
}

export class MinLogicDeclare extends MinLogicStatement {
  constructor(
    public id: string,
    public args: Array<string>,
    public list: MinLogicInstructionList
  ) {
    super();
  }
}

export class MinLogicDeclareInline extends MinLogicDeclare {
  constructor(
    public id: string,
    public args: Array<string>,
    public list: MinLogicInstructionList,
    public returnVar: MinLogicReference
  ) {
    super(id, args, list);
  }
  compile(): string {
    return null;
  }
}
export class MinLogicInlineExpression extends MinLogicStatement {
  public target;
  constructor(
    public outputVar: MinLogicReference,
    public functionName: string,
    public args: Array<MinLogicLiteral>
  ) {
    super();
  }
  compile(): string {
    return null;
  }
  expand(source: MinLogicDeclareInline): MinLogicInstructionList {
    let copiedList = source.list;
    console.log(`functionName ${this.functionName} -- ${copiedList}`);
    console.log(
      `functionName ${this.functionName} -- ${copiedList.constructor.name}`
    );

    copiedList.forEach((i) => {
      for (const [k, ref] of Object.entries(i)) {
        if (ref instanceof MinLogicReference) {
          console.log(`ref ${ref}`);
          if (k == 'outputVar') {
            // Replace function-scoped variables with 'hidden' identifiers
            i[k] = new MinLogicReference(`~${this.functionName}~${ref.value}`);
          } else if (source.args.includes(ref.value)) {
            // Replace argument variables
            let argPosition = source.args.indexOf(ref.value);
            i[k] = this.args[argPosition];
          }
        }
      }
    });

    let lastInstr = copiedList.last();
    if ('outputVar' in lastInstr) {
      // @ts-ignore
      lastInstr.outputVar = this.outputVar;
    }
    return copiedList;
  }
}

export class MinLogicInstructionList extends Array<MinLogicStatement> {
  public symbols = new Map<string, MinLogicDeclare>();

  constructor(statement?: MinLogicStatement) {
    super();
    if (statement) this.push(statement);
  }
  declare(symbol: string, list: MinLogicDeclare) {
    if (this.symbols.has(symbol)) throw `Duplicate symbol definition ${symbol}`;
    this.symbols.set(symbol, list);
  }
  merge(what: MinLogicInstructionList | MinLogicStatement) {
    if (what instanceof MinLogicInstructionList) {
      what.forEach((i) => this.push(i));
      // merge symbols
      what.symbols.forEach((value, key) => {
        if (this.symbols.has(key))
          throw `Duplicate symbol definition in this:\n${this} \nwith:\n${what}`;
        this.symbols.set(key, value);
      });
    } else if (what instanceof MinLogicStatement) {
      if (what instanceof MinLogicDeclareInline) {
        // Do not add to the code list, add as a reference
        this.declare(what.id, what);
      } else {
        // add to the code list
        this.push(what);
      }
    }
    return this;
  }
  clone() {
    return cloneDeep(this);
  }
  last(): MinLogicStatement {
    return this[this.length - 1];
  }
  toString() {
    return `[\n${this.join(',\n')}\n]`;
  }
  expandInlineFunctions() {
    for (let i = 0; i < this.length; i++) {
      let instr = this[i];
      if (instr instanceof MinLogicInlineExpression)
        if (this.symbols.has(instr.functionName)) {
          let source = this.symbols.get(instr.functionName);
          //   let clone = source.clone() as MinLogicDeclareInline;
          let clone = cloneDeep(source as MinLogicDeclareInline);
          this.splice(i, 1, ...instr.expand(clone));
        } else {
          throw `unknown symbol ${instr.functionName}`;
        }
    }
  }
  computeLineNumbers() {
    this.expandInlineFunctions();
    for (let i = 0; i < this.length; i++) {
      this[i].lineNumber = i;
    }
  }
  compile(): string {
    this.computeLineNumbers();
    var output = '';
    this.forEach((instr) => {
      let c = instr.compile();
      if (c) output += instr.compile() + '\n';
    });
    return output;
  }
}

export class MinLogicRead extends MinLogicStatement {
  constructor(
    public varName: MinLogicReference,
    public cellName,
    public index
  ) {
    super();
  }
  compile(): string {
    return `read ${this.varName.value} ${this.cellName.value} ${this.index.value}`;
  }
}

export class MinLogicWrite extends MinLogicStatement {
  constructor(
    public varName: MinLogicReference,
    public cellName,
    public index
  ) {
    super();
  }
  compile(): string {
    return `write ${this.varName.value} ${this.cellName.value} ${this.index.value}`;
  }
}

export class MinLogicDraw extends MinLogicStatement {
  constructor(public command, public args: Array<string>) {
    super();
  }
  compile(): string {
    return `draw ${this.command.value} ${this.args.join(' ')}`;
  }
}

export class MinLogicPrint extends MinLogicStatement {
  constructor(public what: MinLogicString | MinLogicReference) {
    super();
  }
  compile(): string {
    return `print ${this.what.value}`;
  }
}

export class MinLogicDrawFlush extends MinLogicStatement {
  constructor(public displayName: string) {
    super();
  }
  compile(): string {
    return `drawflush ${this.displayName}`;
  }
}

export class MinLogicPrintFlush extends MinLogicStatement {
  constructor(public displayName: string) {
    super();
  }
  compile(): string {
    return `printflush ${this.displayName}`;
  }
}

export class MinLogicLink extends MinLogicStatement {
  constructor(public varName: MinLogicReference) {
    super();
  }
  compile(): string {
    return `getlink ${this.varName.value}`;
  }
}

export class MinLogicControl extends MinLogicStatement {
  constructor(public command, public args: Array<string>) {
    super();
  }
  compile(): string {
    return `control ${this.command.value} ${this.args.join(' ')}`;
  }
}
export class MinLogicRadar extends MinLogicStatement {
  constructor(
    public target,
    public and1,
    public and2,
    public distance: MinLogicNumber,
    public from: MinLogicReference,
    public order: MinLogicNumber,
    public outputVar: MinLogicReference
  ) {
    super();
  }
  compile(): string {
    return (
      `radar ${this.target} ${this.and1} ${this.and2} ${this.distance.value}` +
      ` ${this.from.value} ${this.order.value} ${this.outputVar.value}`
    );
  }
}
export class MinLogicSensor extends MinLogicStatement {
  constructor(
    public outputVar: MinLogicReference,
    public type: MinLogicReference,
    public block: MinLogicReference
  ) {
    super();
  }
  compile(): string {
    return `sensor ${this.outputVar.value} ${this.type.value} ${this.block.value}`;
  }
}

export class MinLogicSet extends MinLogicStatement {
  constructor(public outputVar: MinLogicReference, public setValue) {
    super();
  }
  compile(): string {
    return `set ${this.outputVar.value} ${this.setValue.value}`;
  }
}

export class MinLogicOperation extends MinLogicStatement {
  constructor(
    public outputVar: MinLogicReference,
    public type: string,
    public op1,
    public op2
  ) {
    super();
  }
  compile(): string {
    return `op ${this.type} ${this.outputVar.value} ${this.op1.value} ${this.op2.value}`;
  }
}

export class MinLogicEnd extends MinLogicStatement {
  constructor() {
    super();
  }
  compile(): string {
    return `end`;
  }
}

export class MinLogicJump extends MinLogicStatement {
  constructor(
    public target: MinLogicStatement,
    public type,
    public op1,
    public op2,
    public offset: number = +1
  ) {
    super();
  }
  compile(): string {
    return `jump ${this.target.lineNumber + this.offset} ${this.type} ${
      this.op1.value
    } ${this.op2.value}`;
  }
}

export class MinLogicUnitBind extends MinLogicStatement {
  constructor(public type: string) {
    super();
  }
  compile(): string {
    return `ubind ${this.type}`;
  }
}

export class MinLogicUnitControl extends MinLogicStatement {
  constructor(public command, public args: Array<string>) {
    super();
  }
  compile(): string {
    return `ucontrol ${this.command.value} ${this.args.join(' ')}`;
  }
}

export class MinLogicUnitRadar extends MinLogicStatement {
  constructor(
    public target,
    public and1,
    public and2,
    public order: MinLogicNumber,
    public sort: string,
    public outputVar: MinLogicReference
  ) {
    super();
  }
  compile(): string {
    return (
      `uradar ${this.target} ${this.and1} ${this.and2}` +
      ` ${this.order.value} 0 ${this.sort} ${this.outputVar.value}`
    );
  }
}

export class MinLogicUnitLocate extends MinLogicStatement {
  constructor(
    public find,
    public type,
    public enemy,
    public outX: MinLogicReference,
    public outY: MinLogicReference,
    public found,
    public outputVar: MinLogicReference
  ) {
    super();
  }
  compile(): string {
    return (
      `ulocate ${this.find} ${this.type} ${this.enemy} 0 ${this.outX.value}` +
      ` ${this.outY.value} ${this.found} ${this.outputVar.value}`
    );
  }
}
