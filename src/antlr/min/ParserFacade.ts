// / <reference path="../../../node_modules/ngx-monaco-editor/monaco.d.ts" />

import {
  ANTLRErrorListener,
  CharStreams,
  CommonTokenStream,
  RecognitionException,
  Recognizer,
  Token,
} from 'antlr4ts';
import { MinLangLexer } from './c/MinLangLexer';
import { MinLangParser } from './c/MinLangParser';

// RecognitionException
// class MyErrorListener extends ConsoleErrorListener {
//   syntaxError(recognizer, offendingSymbol, line, column, msg, e) {
//     console.log('ERROR ' + msg);
//   }
// }

export class Error implements monaco.editor.IMarkerData {
  constructor(
    public severity: monaco.MarkerSeverity,
    public startLineNumber: number,
    public endLineNumber: number,
    public startColumn: number,
    public endColumn: number,
    public message: string
  ) {}
}

class MinLangErrorListener implements ANTLRErrorListener<Token> {
  private errors: Error[] = [];

  constructor(errors: Error[]) {
    this.errors = errors;
  }

  syntaxError(
    recognizer: Recognizer<Token, any>,
    offendingSymbol: Token,
    line: number,
    column: number,
    msg: string,
    e: RecognitionException
  ) {
    var endColumn = column + 1;
    if (offendingSymbol.text !== null) {
      endColumn = column + offendingSymbol.text.length;
    }
    this.errors.push(
      new Error(monaco.MarkerSeverity.Error, line, line, column, endColumn, msg)
    );
  }
}

export function createLexer(input: string) {
  let errors: Error[] = [];
  const chars = CharStreams.fromString(input);
  const lexer = new MinLangLexer(chars);
  //   lexer.strictMode = false;
  return lexer;
}

export function getTokens(input: string): Token[] {
  return createLexer(input).getAllTokens();
}

function createParser(input) {
  const lexer = createLexer(input);
  return createParserFromLexer(lexer);
}

function createParserFromLexer(lexer) {
  const tokens = new CommonTokenStream(lexer);
  return new MinLangParser(tokens);
}

function parseTree(input) {
  const parser = createParser(input);
  return parser.parse();
}

export function parseTreeStr(input) {
  let errors: Error[] = [];

  const lexer = createLexer(input);
  lexer.removeErrorListeners();
  //   lexer.addErrorListener(new MinLangErrorListener(errors));

  const parser = createParserFromLexer(lexer);
  parser.removeErrorListeners();
  parser.addErrorListener(new MinLangErrorListener(errors));

  const tree = parser.parse();
  return tree.toStringTree(parser.ruleNames);
}

export function validate(input: monaco.editor.ITextModel): Error[] {
  let errors: Error[] = [];

  const lexer = createLexer(input.getValue());
  lexer.removeErrorListeners();
  //   lexer.addErrorListener(new MinLangErrorListener(errors));

  const parser = createParserFromLexer(lexer);
  parser.removeErrorListeners();
  parser.addErrorListener(new MinLangErrorListener(errors));
  // parser._errHandler = new CalcErrorStrategy();

  const tree = parser.parse();
  return errors;
}
