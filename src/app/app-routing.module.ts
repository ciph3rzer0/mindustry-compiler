import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CodeFileGuard } from 'src/app/data/guards/code-file.guard';
import { CodeEditorComponent } from './code-editor/code-editor.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: '/code',
    pathMatch: 'full',
  },
  {
    path: 'code',
    component: CodeEditorComponent,
    data: { title: 'Code Editor' },
    canActivate: [CodeFileGuard],
  },
  {
    path: 'code/:fileId',
    component: CodeEditorComponent,
    pathMatch: 'full',
    canActivate: [CodeFileGuard],
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
