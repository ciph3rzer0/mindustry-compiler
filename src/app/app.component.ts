import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import * as userActions from 'src/app/data/auth/user.actions';
import { User } from './data/auth/user.model';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit {
  title = 'mindustry';
  constructor(private store: Store<{ user: User }>) {}

  ngOnInit(): void {
    this.store.dispatch(
      userActions.getUser({ origin: 'AppComponent::ngOnInit' })
    );
  }
}
