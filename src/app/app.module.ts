import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { AngularFireModule } from '@angular/fire';
import {
  AngularFireAuthModule,
  USE_EMULATOR as AUTH_EMULATOR,
} from '@angular/fire/auth';
import { USE_EMULATOR as DATABASE_EMULATOR } from '@angular/fire/database';
import { USE_EMULATOR as FIRESTORE_EMULATOR } from '@angular/fire/firestore';
import { USE_EMULATOR as FUNCTIONS_EMULATOR } from '@angular/fire/functions';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ClarityModule } from '@clr/angular';
import { EntityDataModule } from '@ngrx/data';
import { EffectsModule } from '@ngrx/effects';
import {
  RouterStateSerializer,
  StoreRouterConnectingModule,
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { NgeMonacoModule, NGE_MONACO_CONTRIBUTION } from 'nge-monaco';
// import { ButtonModule } from 'primeng/button';
// import { MenuModule } from 'primeng/menu'; // add this import
// import { MenubarModule } from 'primeng/menubar';
import { environment } from 'src/environments/environment';
import { HeaderComponent } from '../app/header/header.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CodeEditorComponent } from './code-editor/code-editor.component';
import { MinLangContribution } from './code-editor/min-lang-contribution';
import { MonacoConfig } from './code-editor/monaco-config';
import { entityConfig } from './data/app-entity-metadata';
import { UserEffects } from './data/auth/user.effects';
import { authReducer } from './data/auth/user.reducer';
import { NgrxDataFirestoreModule } from './data/firebase/ngrx-data-firestore.module';
import { CustomSerializer, reducers } from './data/router';
import { ProfileBadgePresenter } from './header/profile-badge/profile-badge.presenter';
import { VersionSelectWidget } from './widgets/version-select/version-select.widget';
import { WelcomeTabComponent } from './code-editor/welcome-tab/welcome-tab.component';
import { ClipboardModule } from '@angular/cdk/clipboard';
import { DragDropModule } from '@angular/cdk/drag-drop';

@NgModule({
  declarations: [
    AppComponent,
    CodeEditorComponent,
    HeaderComponent,
    ProfileBadgePresenter,
    VersionSelectWidget,
    WelcomeTabComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    NgeMonacoModule.forRoot(MonacoConfig),
    FormsModule,
    ReactiveFormsModule,
    ClarityModule,
    BrowserAnimationsModule,

    AngularFireModule.initializeApp(environment.firebaseConfig),
    AngularFireAuthModule,

    StoreModule.forRoot({
      user: authReducer,
      router: reducers.routerReducer,
    }),
    StoreRouterConnectingModule.forRoot({ serializer: CustomSerializer }),
    EffectsModule.forRoot([UserEffects]),
    EntityDataModule.forRoot(entityConfig),
    StoreDevtoolsModule.instrument({
      maxAge: 25, // Retains last 25 states
      logOnly: environment.production, // Restrict extension to log-only mode
    }),

    NgrxDataFirestoreModule,
    // MenuModule,
    // MenubarModule,
    // ButtonModule,

    ClipboardModule,
    DragDropModule,
  ],
  providers: [
    {
      provide: RouterStateSerializer,
      useClass: CustomSerializer,
    },

    {
      provide: NGE_MONACO_CONTRIBUTION,
      multi: true,
      useClass: MinLangContribution,
    },
    {
      provide: AUTH_EMULATOR,
      useValue: environment.emulated ? ['localhost', 9099] : undefined,
    },
    {
      provide: FIRESTORE_EMULATOR,
      useValue: environment.emulated ? ['localhost', 8080] : undefined,
    },
    {
      provide: DATABASE_EMULATOR,
      useValue: environment.emulated ? ['localhost', 9000] : undefined,
    },
    {
      provide: FUNCTIONS_EMULATOR,
      useValue: environment.emulated ? ['localhost', 5001] : undefined,
    },
  ],
  bootstrap: [AppComponent],
})
export class AppModule {}
