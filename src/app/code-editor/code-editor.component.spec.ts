import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ClarityModule } from '@clr/angular';
import { provideMockStore } from '@ngrx/store/testing';
import { MockService } from 'ng-mocks';
import { NgeMonacoModule } from 'nge-monaco';
import { AppModule } from '../app.module';
import { CodefileService } from '../data/services/codefile.service';
import { CodeEditorComponent } from './code-editor.component';

describe('CodeEditorComponent', () => {
  let component: CodeEditorComponent;
  let fixture: ComponentFixture<CodeEditorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, ClarityModule, FormsModule],
      providers: [
        NgeMonacoModule,
        provideMockStore(),
        {
          provide: CodefileService,
          useValue: MockService(CodefileService),
        },
      ],
      declarations: [CodeEditorComponent],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CodeEditorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
