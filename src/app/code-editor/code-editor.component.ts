import { CdkDragDrop, moveItemInArray } from '@angular/cdk/drag-drop';
import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import firebase from 'firebase/app';
import { Observable } from 'rxjs';
import { first } from 'rxjs/operators';
import { CodefileService } from 'src/app/data/services/codefile.service';
import { CodeParserService } from '../code-parser/code-parser.service';
import { User } from '../data/auth/user.model';
import { Codefile } from '../data/models/codefile.model';

const MINLANG = 'minlang';

interface EditorTab {
  title: string;
  codeFile: Codefile;
  model?: monaco.editor.ITextModel;
  diff?: monaco.editor.IDiffEditorModel;
  dirty?: boolean;
  error?: boolean;
  editorOptions: { readOnly: boolean };
}

@Component({
  selector: 'app-code-editor',
  templateUrl: './code-editor.component.html',
  styleUrls: ['./code-editor.component.scss'],
  host: { class: 'content-container' },
})
export class CodeEditorComponent implements OnInit {
  editorOptions: monaco.editor.IStandaloneEditorConstructionOptions = {
    // theme: 'vs-dark',
    language: MINLANG,
    theme: 'myCoolTheme',
    // scrollBeyondLastLine: true,
  };

  openTabs: EditorTab[] = [];
  selectedTab: EditorTab;

  editorInstance: monaco.editor.ICodeEditor;
  diffEditorInstance: monaco.editor.IDiffEditor;

  isDiffView: boolean = false;
  originalModel: monaco.editor.ITextModel;
  modifiedModel: monaco.editor.ITextModel;
  newFileVersion: string;

  showRenameModal: boolean = false;
  renameFile: string = null;
  renameModelText: string = '';

  constructor(
    private parser: CodeParserService,
    private codefileService: CodefileService,
    private store: Store<{ user: User }>
  ) {}

  code: string = `
# Math expressions with order of operations
y = a * 2 + 3 ^ b;
index = 3 * (1 + cos(2));

# Conditional branching
if (i < 10) {
    i = i * 2;
} else if (i >= 50) {
    i = i - 10;
} else {
    i = i + 5; 
}

# While loops
while (i < 10) {
    i = i + 1;
}

# Print complex strings on one line
name = "steve";
print("hello world ", name, "\\n", 1+3, @time);
printflush(message1);

# Instructions:
read(var1, cell1, 0);
write(var1, cell1, 0);
getlink(newBlockName);

# Native mindustry logic instructions
read var1 cell1 0
set i 5
jump 21 lessThan i 6

# Mix and match both types of code
set cond 1
while(cond == true) {
    print("exiting");
    set cond 0
}

# Inline functions (copys code in place, not a traditional function)
inline add(a, b) {
    return a + b;
}

# Nested functions
add_result = add(add(11, 12) * 3, 13);

# Functions have their own scope, variables are passed by value
inline increment(a) {
    a = a + 1;
}

a = 1;
increment(a); # a is still 1

`.trim();

  user$: Observable<User>;

  showEditorSidebar = false;
  compilerOutput: string;

  history$: Observable<Codefile[]>;

  ngOnInit(): void {
    this.user$ = this.store.select('user');
    this.codefileService.getAll();
    // let id = 'DTel97YiaRSsD3uf7fcr';

    // this.codefileService.fetchCodefileHistory(id);
    // this.history$ = this.codefileService.getCodefileHistory(id);
  }

  /**
   * Callback from the monaco editor when it's initialized.
   * @param editor the monaco editor instance.
   */
  onMonacoInit(editor: monaco.editor.ICodeEditor) {
    console.log('editor init');
    this.editorInstance = editor;
    editor.updateOptions(this.editorOptions);

    // Process firestore documents into monaco models
    // this.codefileService.entities$
    //   .pipe(
    //     filter((arr) => arr.length > 0),
    //     first()
    //   )
    //   .subscribe((arr) => {
    //     arr.forEach((cf) => this.onOpenFile(cf));
    //     this.changeTab(this.openTabs[0]);
    //   });
  }

  /**
   * Callback from the monaco diff editor when it's initialized.
   * @param diffEditor the monaco diff editor instance
   */
  onMonacoDiffInit(diffEditor: monaco.editor.IDiffEditor) {
    console.log('diff editor init');
    this.diffEditorInstance = diffEditor;
    diffEditor.updateOptions(this.editorOptions);
  }

  canCompile() {
    return this.selectedTab?.codeFile;
  }

  /**
   * Convert minlang code to mindustry logic
   */
  onCompile(): void {
    this.compilerOutput = this.parser
      .lex(this.selectedTab.model.getValue())
      .replace('\n', '\r\n');

    this.showEditorSidebar = true;
  }

  /**
   * Creates a new file with the name given in the parameters.  And opens it in the editor
   * @param fileName
   */
  onNewFile(fileName: string) {
    let newFile: any;

    this.user$.pipe(first()).subscribe((user: User) => {
      newFile = {
        // TODO: do this in function
        uid: user.uid,
        displayName: user.displayName,
        photoUrl: user.photoUrl,

        docName: fileName,
        doc: '',
        createDate: firebase.firestore.FieldValue.serverTimestamp(),
        updateDate: firebase.firestore.FieldValue.serverTimestamp(),
        version: '0.0.0',
        versionHistory: '',
      };
    });
    console.log('newFile ', newFile);

    this.codefileService.add(newFile).subscribe((cf: Codefile) => {
      var newTab = this.createEditorTabFromCodefile(
        { ...cf, doc: this.code.slice() },
        null,
        this.createURIFromCodefile(cf, 'editor')
      );

      this.openTabs.push(newTab);
      this.changeTab(newTab);
    });
  }

  canDiffAndSave() {
    return !(
      !this.selectedTab?.codeFile ||
      this.selectedTab?.editorOptions.readOnly ||
      this.selectedTab?.diff
    );
  }

  /**
   * Open a diff editor window with the new changes when the user presses "Save".
   */
  onSaveFile() {
    if (!this.canDiffAndSave()) return;

    const cf = this.selectedTab.codeFile;
    let original = this.createModelFromCodefile(cf);

    let newDiffTab: EditorTab = {
      title: `${this.selectedTab.codeFile.docName} (diff)`,
      codeFile: this.selectedTab.codeFile,
      diff: {
        original: original,
        modified: this.selectedTab.model,
      },
      dirty: true,
      editorOptions: {
        readOnly: false,
      },
    };
    this.openTabs.push(newDiffTab);
    this.changeTab(newDiffTab);
  }

  /**
   * Persist the changes when the user confirms the save.
   */
  onSaveConfirm() {
    let saveDoc = {
      ...this.selectedTab.codeFile,
      doc: this.selectedTab.diff.modified.getValue(),
      version: this.newFileVersion,
    };

    this.codefileService.update(saveDoc);
    this.selectedTab.dirty = false;

    // TODO This seems like a hack, and doesn't work
    // this.selectedTab.codeFile.doc = this.selectedTab.diff.modified.getValue()
  }

  /**
   * Open a codefile as at tab.
   * @param cf the codefile object to open.
   */
  onOpenFile(cf: Codefile) {
    if (this.switchTabsIfFileOpen(cf)) return;

    var newTab = this.createEditorTabFromCodefile(
      cf,
      null,
      this.createURIFromCodefile(cf, 'editor')
    );

    this.openTabs.push(newTab);
    this.changeTab(newTab);
  }

  /**
   * Close an editor tab.
   * @param tab Optional tab to close.  The current tab by default.
   */
  onCloseTab(tab: EditorTab = this.selectedTab) {
    let index = this.openTabs.indexOf(tab);
    this.openTabs.splice(index, 1);

    // TODO: Verify model is not being used in other tabs.
    if (tab.model) tab.model.dispose();

    if (this.selectedTab == tab) {
      if (this.openTabs.length > index) {
        this.changeTab(this.openTabs[index]);
      } else if (this.openTabs.length > 0) {
        this.changeTab(this.openTabs[index - 1]);
      } else {
        this.selectedTab = null;
      }
    }
  }

  onTabDragDrop(event: CdkDragDrop<Codefile[]>) {
    moveItemInArray(this.openTabs, event.previousIndex, event.currentIndex);
  }

  /**
   * Change the contents of the editor to display a text or diff model.
   * @param tab The tab object to display in the editor window
   */
  changeTab(tab: EditorTab) {
    this.selectedTab = tab;
    if (tab.model !== undefined) {
      this.editorInstance.setModel(tab.model);
      this.editorInstance.updateOptions(tab.editorOptions);
    } else if (tab.diff !== undefined) {
      this.diffEditorInstance.setModel(tab.diff);
      this.diffEditorInstance.updateOptions(tab.editorOptions);
    }

    let history = this.selectedTab.codeFile.parent
      ? this.selectedTab.codeFile.parent
      : this.selectedTab.codeFile.id;

    this.codefileService.fetchCodefileHistory(history);
    this.history$ = this.codefileService.getCodefileHistory(history);
  }

  newTab() {
    this.selectedTab = null;
  }

  canRename() {
    return this.selectedTab?.codeFile?.docName;
  }
  onClickRename() {
    if (this.canRename()) this.showRenameModal = true;
  }
  closeRenameModal() {
    this.showRenameModal = false;
    this.renameModelText = '';
  }
  renameCurrentFile() {
    let newCodefile = {
      ...this.selectedTab.codeFile,
      docName: this.renameModelText,
    };

    this.codefileService.updateOneInCache(newCodefile);

    this.selectedTab.title = this.renameModelText;
    this.selectedTab.codeFile = newCodefile;

    this.showRenameModal = false;
    this.renameModelText = '';
  }

  /**
   * Callback from the version select widget.
   */
  setNewFileVersion(event) {
    this.newFileVersion = event;
  }

  /**
   * Opens a past codefile version in as a readonly tab in the editor.
   * @param fileVersion the versioned codefile.
   */
  async openVersion(fileVersion: Codefile) {
    if (this.switchTabsIfFileOpen(fileVersion)) return;

    let parent = await this.codefileService
      .getByKey(fileVersion.parent)
      .toPromise();

    let newTab = this.createEditorTabFromCodefile(
      fileVersion,
      `${parent.docName} -- v${fileVersion.version}`
    );
    newTab.editorOptions.readOnly = true;

    this.openTabs.push(newTab);
    this.changeTab(newTab);
  }

  /**
   * Loops through every tab to see if the file is open in a tab.  Switches and
   * returns true if one is found.
   * @param cf The codefile to search for.
   */
  switchTabsIfFileOpen(cf: Codefile) {
    let isOpen = false;

    this.openTabs.forEach((tab) => {
      if (tab.codeFile.id == cf.id) {
        this.changeTab(tab);
        isOpen = true;
        return;
      }
    });

    return isOpen;
  }

  /**
   * Creates an EditorTab from a codefile.
   * @param cf The Codefile to create a tab for
   * @param title Optional header for the editor tab.  Defaults to the filename
   */
  createEditorTabFromCodefile(
    cf: Codefile,
    title: string = null,
    uri: monaco.Uri = null
  ) {
    return {
      title: title || cf.docName,
      codeFile: cf,
      model: this.createModelFromCodefile(cf, uri),
      dirty: false,
      editorOptions: {
        readOnly: false,
      },
    };
  }

  /**
   * Creates a monaco model from a codefile.
   * @param cf - The codefile to convert.
   * @param uri Optional custom URI.
   */
  createModelFromCodefile(cf: Codefile, uri: monaco.Uri = null) {
    uri = uri || this.createURIFromCodefile(cf);
    let getModel = monaco.editor.getModel(uri);

    if (getModel) return getModel;
    return monaco.editor.createModel(cf.doc, MINLANG, uri);
  }

  /**
   * Create a URI based on a codefile's name, version, and user.
   * @param cf The codefile to generate the URI from.
   * @param version an optional version override string.
   */
  createURIFromCodefile(cf: Codefile, version: string = null) {
    return monaco.Uri.from({
      scheme: MINLANG,
      authority: 'mindustry-compiler.web.app',
      path: `/${cf.docName}`,
      query: `v=${version || cf.version}&u=${cf.displayName}`,
      fragment: `${cf.id}`,
    });
  }
}
