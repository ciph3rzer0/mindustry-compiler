import { Injectable } from '@angular/core';
import {} from 'monaco-editor';
import { NgeMonacoContribution } from 'nge-monaco';
import { MinLangTokensProvider } from '../../antlr/min/MinLangTokensProvider';
import { validate } from '../../antlr/min/ParserFacade';

@Injectable()
export class MinLangContribution implements NgeMonacoContribution {
  constructor() {}

  activate(): void | Promise<void> {
    monaco.languages.register({ id: 'minlang' });
    monaco.languages.setTokensProvider('minlang', new MinLangTokensProvider());

    monaco.languages.registerCompletionItemProvider('minlang', {
      provideCompletionItems: (model, position, context, token) => {
        var suggestions = [
          {
            label: 'ifelse',
            kind: monaco.languages.CompletionItemKind.Snippet,
            insertText: [
              'if (${1:condition}) {',
              '\t$0',
              '} else {',
              '\t',
              '}',
            ].join('\n'),
            insertTextRules:
              monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
            documentation: 'If-Else Statement',
          },
          {
            label: 'while',
            kind: monaco.languages.CompletionItemKind.Snippet,
            insertText: ['while (${1:condition}) {', '\t$0', '}'].join('\n'),
            insertTextRules:
              monaco.languages.CompletionItemInsertTextRule.InsertAsSnippet,
            documentation: 'While loop',
          },
        ];
        return {
          suggestions: suggestions,
        } as monaco.languages.CompletionList;
      },
    });

    monaco.editor.onDidCreateModel((model) => {
      model.onDidChangeContent((changedEvent) => {
        let errors = validate(model);
        monaco.editor.setModelMarkers(model, 'Example', errors);
      });
    });

    //   let identifier = '#008DD5'.substring(1);
    //   let idFg = '#344482'.substring(1);
    //   let keywordFg = '#7132a8'.substring(1);
    let variable = '#9CDCFE'.substring(1);
    let errorFg = '#ff0000'.substring(1);
    let blockFg = '#C586C0'.substring(1);
    let commentFg = '#6A9955'.substring(1);
    let functionFg = '#569CD6'.substring(1);
    let methodFg = '#DCDCAA'.substring(1);
    let mathFg = '#DCDCAA'.substring(1);
    let numberFg = '#B5CEA8'.substring(1);

    monaco.editor.defineTheme('myCoolTheme', {
      base: 'vs-dark',
      inherit: true,
      rules: [
        { token: 'block', foreground: blockFg },
        { token: 'comment', foreground: commentFg },
        { token: 'method', foreground: methodFg },
        { token: 'function', foreground: functionFg },
        { token: 'math_function', foreground: mathFg, fontStyle: 'italic' },
        { token: 'variable', foreground: variable },

        { token: 'boolean', foreground: numberFg },
        { token: 'number', foreground: numberFg },

        { token: 'delimiter', foreground: '9448BC' },

        { token: 'undefined', foreground: errorFg },
        { token: 'error', foreground: errorFg },
      ],
      colors: {},
    } as monaco.editor.IStandaloneThemeData);
  }

  deactivate(): void | Promise<void> {
    // free the disposables and subscriptions here
  }
}
