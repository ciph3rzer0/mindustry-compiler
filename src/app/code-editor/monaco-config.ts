import { NgeMonacoConfig } from 'nge-monaco/lib/monaco-config';

export const MonacoConfig: NgeMonacoConfig = {
  options: {
    scrollBeyondLastLine: false,
    automaticLayout: true,
  } as monaco.editor.IEditorOptions,
};
