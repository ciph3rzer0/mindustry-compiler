import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClarityModule } from '@clr/angular';
import { provideMockStore } from '@ngrx/store/testing';
import { MockService } from 'ng-mocks';
import { AppModule } from 'src/app/app.module';
import { CodefileService } from 'src/app/data/services/codefile.service';
import { WelcomeTabComponent } from './welcome-tab.component';

describe('WelcomeTabComponent', () => {
  let component: WelcomeTabComponent;
  let fixture: ComponentFixture<WelcomeTabComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule, ClarityModule],
      declarations: [WelcomeTabComponent],
      providers: [
        provideMockStore(),
        {
          provide: CodefileService,
          useValue: MockService(CodefileService),
        },
      ],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WelcomeTabComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
