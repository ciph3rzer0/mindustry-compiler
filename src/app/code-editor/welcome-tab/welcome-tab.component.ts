import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Store } from '@ngrx/store';
import { combineLatest, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { User } from 'src/app/data/auth/user.model';
import { Codefile } from 'src/app/data/models/codefile.model';
import { CodefileService } from 'src/app/data/services/codefile.service';

@Component({
  selector: 'app-welcome-tab',
  templateUrl: './welcome-tab.component.html',
  styleUrls: ['./welcome-tab.component.scss'],
})
export class WelcomeTabComponent implements OnInit {
  @Output() selectFile = new EventEmitter<Codefile>();
  @Output() newFile = new EventEmitter<string>();

  user$: Observable<User>;
  showUsersFiles = new FormControl();

  files$: Observable<Codefile[]>;

  newFileName: string;

  constructor(
    private codefileService: CodefileService,
    private store: Store<{ user: User }>
  ) {}

  ngOnInit(): void {
    this.user$ = this.store.select('user');

    this.files$ = combineLatest([
      this.codefileService.getAllCodefiles(),
      this.showUsersFiles.valueChanges,
      this.user$,
    ]).pipe(
      map(([files, c, user]) => files.filter((f) => !c || f.uid == user.uid))
    );

    // combineLatest only works if all streams have input.  For some reason
    // this only works with a delay.  Doing it in ngViewAfterInit results in
    // 'ExpressionChangedAfterItHasBeenCheckedError' errors
    new Promise((resolve) => setTimeout(resolve, 5)).then(() =>
      this.showUsersFiles.setValue(false)
    );
  }

  onFileSelect(cf: Codefile): void {
    this.selectFile.emit(cf);
  }

  onNewFileClicked() {
    this.newFile.emit(this.newFileName);
  }
}
