import { Injectable } from '@angular/core';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MinLangLexer } from 'src/antlr/min/c/MinLangLexer';
import { MinLangParser } from 'src/antlr/min/c/MinLangParser';
import { MinLangExecutor } from 'src/antlr/min/MinLangVisitor';
import { MinLogicInstructionList } from 'src/antlr/min/MinLogic';

// import { lex } from './lex-module';
// import { parse } from './parser-module';
// import * as actions from './actions-visitor';
// import { strict as assert } from 'assert';

@Injectable({
  providedIn: 'root',
})
export class CodeParserService {
  constructor() {}

  lex(inputText: string) {
    // Create the lexer and parser
    let inputStream = CharStreams.fromString(inputText);
    let lexer = new MinLangLexer(inputStream);
    let tokenStream = new CommonTokenStream(lexer);

    console.log(tokenStream.getTokens());
    console.log(tokenStream.getTokens().toString());
    console.log(tokenStream.getText());

    // tokenStream.tokenSource.

    let parser = new MinLangParser(tokenStream);
    // Parse the input, where `compilationUnit` is whatever entry point you defined
    let tree = parser.parse();

    let exe = new MinLangExecutor();
    let result = exe.visit(tree);

    return (result as MinLogicInstructionList).compile();
  }
}
