import { EntityMetadataMap } from '@ngrx/data';

const entityMetadata: EntityMetadataMap = {
    Codefile: {},
};

const pluralNames = {};

export const entityConfig = {
    entityMetadata,
    pluralNames,
};
