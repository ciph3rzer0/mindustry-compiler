import { createAction, props } from '@ngrx/store';
import { User } from './user.model';

/** Check if user data is cached. */
export const getUser = createAction(
    '[User] Get User Auth State',
    props<{ origin?: any }>()
);

/** Send a request to google to prompt user to login. */
export const userGoogleLogin = createAction(
    '[User/API] *Action* Google Login',
    props<{ origin?: any }>()
);
export const userAuthenticated = createAction(
    '[User] Authenticated',
    props<{ user: User }>()
);
export const userNotAuthenticated = createAction(
    '[User] Not Authenticated'
    // props<{ payload?: any }>()
);
export const userAuthError = createAction(
    '[User] Authenticate User Error',
    props<{ error: any }>()
);

/** Delete local credentials.  No API calls needed, I think.*/
export const userLogout = createAction(
    '[User] *Action* Logout',
    props<{ origin?: any }>()
);
export const userLogoutComplete = createAction(
    '[User] Logout Complete'
    // props<{ payload?: any }>()
);
export const userLogoutError = createAction(
    '[User] Logout Error',
    props<{ error: any }>()
);
