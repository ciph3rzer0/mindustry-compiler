import { TestBed } from '@angular/core/testing';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { provideMockActions } from '@ngrx/effects/testing';
import { MockStore, provideMockStore } from '@ngrx/store/testing';
import { MockService } from 'ng-mocks';
import { Observable } from 'rxjs';
import { UserEffects } from './user.effects';

describe('UserEffects', () => {
    let actions$: Observable<any>;
    let effects: UserEffects;
    let mockStore: MockStore;

    beforeEach(() => {
        TestBed.configureTestingModule({
            providers: [
                provideMockStore(),
                provideMockActions(() => actions$),
                UserEffects,
                {
                    provide: AngularFirestore,
                    useValue: MockService(AngularFirestore),
                },
                {
                    provide: AngularFireAuth,
                    useValue: MockService(AngularFireAuth),
                },
            ],
        });

        // mockStore = TestBed.inject(MockStore);
        effects = TestBed.inject(UserEffects);
    });

    it('should be created', () => {
        expect(effects).toBeTruthy();
    });
});
