import { Injectable } from '@angular/core';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { Actions, createEffect, ofType } from '@ngrx/effects';
import firebase from 'firebase/app';
import { from, of } from 'rxjs';
import { catchError, exhaustMap, map, switchMap } from 'rxjs/operators';
import * as userActions from './user.actions';
import { User } from './user.model';

@Injectable()
export class UserEffects {
    constructor(
        private actions$: Actions,
        public firestore: AngularFirestore,
        public afAuth: AngularFireAuth
    ) {}

    /** Called once, map switches to authstate where it will react to the auth
     *  state as it changes.
     */
    getUser$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(userActions.getUser),
            switchMap(
                // The authState observable stays alive until the user logs out.
                // Meaning userNotAuthenticated will be dispatched automatically.
                (_action) =>
                    this.afAuth.authState.pipe(
                        map((authData) => {
                            if (authData) {
                                const user = {
                                    uid: authData.uid,
                                    displayName: authData.displayName,
                                    email: authData.email,
                                    photoUrl: authData.photoURL,
                                } as User;
                                return userActions.userAuthenticated({ user });
                            } else {
                                return userActions.userNotAuthenticated();
                            }
                        }),
                        catchError((error) =>
                            of(userActions.userAuthError({ error }))
                        )
                    )
            )
        );
    });

    userGoogleLogin$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(userActions.userGoogleLogin),
            exhaustMap((_action) =>
                from(this.googleLogin()).pipe(
                    map((_credential) => {
                        return userActions.getUser({
                            origin: 'userGoogleLogin',
                        });
                    }),
                    catchError((error) =>
                        of(userActions.userAuthError({ error }))
                    )
                )
            )
        );
    });

    userLogout$ = createEffect(() => {
        return this.actions$.pipe(
            ofType(userActions.userLogout),
            exhaustMap((_action) =>
                of(this.afAuth.signOut()).pipe(
                    map((_authData) => {
                        return userActions.userLogoutComplete();
                    }),
                    catchError((error) =>
                        of(userActions.userAuthError({ error }))
                    )
                )
            )
        );
    });

    /// Helper Methods
    private googleLogin() {
        const provider = new firebase.auth.GoogleAuthProvider();
        return this.afAuth.signInWithPopup(provider);
    }
}
