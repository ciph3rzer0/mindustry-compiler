import { createReducer, on } from '@ngrx/store';
import * as userActions from './user.actions';
import { User } from './user.model';

export const initialState: User = {
    uid: null,
    displayName: 'Guest',
    email: null,
    photoUrl: null,
};

export const authReducer = createReducer(
    initialState,
    on(userActions.getUser, (state, action) => ({ ...state, loading: true })),

    on(userActions.userGoogleLogin, (state, action) => ({
        ...state,
        loading: true,
    })),
    on(userActions.userAuthenticated, (state, action) => ({
        ...state,
        ...action.user,
        loading: false,
    })),
    on(
        userActions.userNotAuthenticated,
        userActions.userLogoutComplete,
        (state, action) => ({ ...initialState, loading: false })
    ),
    on(
        userActions.userAuthError,
        userActions.userLogoutError,
        (state, action) => ({
            ...initialState,
            error: action.error,
            loading: false,
        })
    ),

    on(userActions.userLogout, (state, action) => ({ ...state, loading: true }))
);
