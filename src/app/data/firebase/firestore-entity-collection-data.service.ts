import { Injectable } from '@angular/core';
import {
  AngularFirestore,
  AngularFirestoreCollection,
  DocumentChangeAction,
  DocumentReference,
  Query,
} from '@angular/fire/firestore';
import {
  EntityCollectionDataService,
  Pluralizer,
  QueryParams,
} from '@ngrx/data';
import { Update } from '@ngrx/entity';
import { from, Observable } from 'rxjs';
import { map, mergeMap } from 'rxjs/operators';

type BaseEntity = { id?: string; uid?: string };

@Injectable({ providedIn: 'root' })
export class FirestoreEntityCollectionDataService<T extends BaseEntity>
  implements EntityCollectionDataService<T | DocumentChangeAction<T>> {
  constructor(
    private collection: AngularFirestoreCollection<T>,
    private firestore: AngularFirestore
  ) {}

  public get name() {
    return this.collection.ref.path;
  }

  // TODO error handling:
  add(entity: T): Observable<T> {
    // return from(this.collection.add(entity)).pipe(
    //     map((docRef: DocumentReference) => ({ ...entity, id: docRef.id }))
    // );

    // Remove ID from entity before saving
    let { id, ...saveEntity } = entity;

    console.log('FIRESTORE-ECS ', saveEntity);

    // let path = `users/${uid}/${this.name}`;
    // console.log('path ', path);

    // let collection = this.firestore.collection<T>(path);
    // console.log('collection ', collection);

    // return from(collection.add(entity)).pipe(
    //     mergeMap((doc: DocumentReference) => this.getById(doc.id))
    // );

    if (id != null) {
      console.log('does have id: ', id);
      throw new Error('Method not implemented.  Test this out');
      // collection.doc(id).set(saveEntity as T);
      // return this.getById(id);
    }

    return from(this.collection.add(saveEntity as T)).pipe(
      mergeMap((doc: DocumentReference) => this.getById(doc.id))
    );
  }

  delete(key: string | number): Observable<string | number> {
    // TODO set "trash" flag
    // return from(this.collection.doc(id).delete());

    return from(this.collection.doc('' + key).delete()).pipe(map(() => key));
  }

  getAll(): Observable<T[]> {
    // throw new Error('Method not implemented.');
    return this.collection.valueChanges({ idField: 'id' });
  }

  getById(id: any): Observable<T> {
    return this.collection.doc<T>(id).valueChanges({ idField: 'id' });
  }

  getWithQuery(params: string | QueryParams): Observable<T[]> {
    console.log(`getWithQuery:params<${this.name}>:`, params);

    /**
     * Construction a firabase query based on the params passed into this function
     * @param ref The root collection
     */
    let queryFunction = (ref) => {
      let query: Query<T> = ref;
      // | CollectionReference<DocumentData> = ref;

      // Filter by submitted user
      if (params['uid']) {
        console.log('uid: ', params['uid']);
        query = query.where('uid', '==', params['uid']);
      }

      if (params['order']) {
        console.log('order: ', params['order']);
        query = query.orderBy(params['order']);
      }

      // Query for comments related to page
      if (params['page']) {
        console.log('page: ', params['page']);
        query = query.where('page', '==', params['page']);
      }

      if (params['where']) {
        for (let p in params['where']) {
          console.log('where: ', p);
          let v = params['where'].split(' ');

          if (v.length == 3) {
            query = query.where(v[0], v[1], v[2]);
          } else {
            console.log(`ERROR: invalid where clause: ${v}`);
          }
        }
      }

      query = query.where('vis', '!=', 'private');
      // if (params['public'] != undefined) {
      //     console.log(
      //         'public: ',
      //         params['public'] != undefined ? true : false
      //     );
      //     query = query.where('vis', '!=', 'private');
      // }
      // console.log('query, ', query);
      return query;
    };

    /**
     * Convert versioned documents to a format friendly to the ngrx store
     * @param arr Array to map over
     */
    const mapVersionedDocument = (arr) =>
      arr.map((val: any) => {
        let mapVal = {
          ...val,
          id: params['history'] + val.version,
          parent: params['history'],
        };

        // console.log('Converting val: ', val, ' to ', mapVal);
        return mapVal;
      });

    // Get document history
    if (params['history']) {
      console.log('history of: ', params['history']);
      if (params['version']) {
        console.log('version: ', params['version']);
        // Retreive a single version
        return this.firestore
          .collection<T>(this.name)
          .doc(params['history'])
          .collection('_history')
          .doc(params['version'])
          .valueChanges()
          .pipe(map(mapVersionedDocument));
      } else {
        // Retrieve the whole history.
        return this.firestore
          .collection<T>(this.name)
          .doc(params['history'])
          .collection('_history')
          .valueChanges()
          .pipe(map(mapVersionedDocument));
      }
    }

    // // Filter by submitted user
    // if (params['uid']) {
    //     console.log('uid: ', params['uid']);

    //     let path = `users/${params['uid']}/${this.name}`;
    //     console.log('path ', path);

    //     let collection = this.firestore.collection<T>(path, queryFunction);
    //     console.log('collection ', collection);
    //     return collection.valueChanges({ idField: 'id' });
    // }

    // // Query a collection group if necessary
    // if (params['group']) {
    //     return this.firestore
    //         .collectionGroup<T>(this.name, queryFunction)
    //         .valueChanges({ idField: 'id' });
    // }
    // // Else, normal colleciton query
    // else {

    // let path = `users/${params['uid']}/${this.name}`;
    // console.log('path ', path);
    return this.firestore
      .collection<T>(this.name, queryFunction)
      .valueChanges({ idField: 'id' });
  }

  update(update: Update<T>): Observable<T> {
    const id = String(update.id);
    const updateDoc = this.collection.doc(id);

    return from(updateDoc.update(update.changes)).pipe(
      mergeMap(() => <Observable<T>>updateDoc.valueChanges())
    );
  }

  upsert(entity: T): Observable<T> {
    throw new Error('Method not implemented.');
  }
}
/**
 * Create a data-service for a single firestore collection.
 */
@Injectable()
export class FirestoreDataServiceFactory {
  constructor(
    protected firestore: AngularFirestore,
    protected plural: Pluralizer
  ) {}

  /**
   * Create a default {EntityCollectionDataService} for the given entity type
   * @param entityName {string} Name of the entity type for this data service
   */
  create<T>(
    entityName: string
  ): EntityCollectionDataService<T | DocumentChangeAction<T>> {
    return new FirestoreEntityCollectionDataService<T>(
      this.firestore.collection(
        this.plural.pluralize(entityName).toLowerCase()
      ),
      this.firestore
    );
  }
}
