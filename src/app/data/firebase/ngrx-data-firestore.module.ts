import { NgModule } from '@angular/core';
import {
    DefaultDataService,
    DefaultDataServiceFactory,
    PersistenceResultHandler,
    EntityDataModule,
} from '@ngrx/data';
import { FirestoreDataServiceFactory } from './firestore-entity-collection-data.service';
import { FirestorePersistenceResultHandler } from './firestore-persistence-result-handler.service';
import { AngularFirestoreModule } from '@angular/fire/firestore';

@NgModule({
    imports: [EntityDataModule, AngularFirestoreModule],
    providers: [
        {
            provide: DefaultDataServiceFactory,
            useClass: FirestoreDataServiceFactory,
        },
        // https://ngrx.io/guide/data/entity-metadata#additionalcollectionstate
        // {
        //     provide: PersistenceResultHandler,
        //     useClass: FirestorePersistenceResultHandler,
        // },
    ],
})
export class NgrxDataFirestoreModule {}
