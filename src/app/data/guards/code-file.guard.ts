import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, UrlTree } from '@angular/router';
import { select } from '@ngrx/store';
import { Observable, of } from 'rxjs';
import { catchError, filter, switchMap, take, tap } from 'rxjs/operators';
import { CodefileService } from '../services/codefile.service';

@Injectable({
  providedIn: 'root',
})
export class CodeFileGuard implements CanActivate {
  constructor(private codeFileService: CodefileService) {}

  canActivate(next: ActivatedRouteSnapshot): Observable<boolean | UrlTree> {
    const { fileId } = next.params;
    let guard: Observable<boolean | UrlTree | any>;

    return of(true);
    if (!fileId) {
      guard = this.guardAllCodeFiles();
    } else {
      guard = this.guardOneCodeFile(fileId);
    }

    return guard.pipe(
      switchMap(() => of(true)),
      catchError((err) => {
        console.log('Recipe Guard Error: ', err);
        return of(false);
      })
    );
  }

  /** Sets up the observable chain.  Firestore should keep in sync after. */
  guardAllCodeFiles(): Observable<boolean | UrlTree | any> {
    return this.codeFileService.count$.pipe(
      // if codeFiles not loaded, dispatch an API action
      tap((count: number) => {
        // TODO Could this be done without the conditional?  Behavior Subject or something
        if (!count) {
          this.codeFileService.getAll();
        }
      }),
      filter((count) => !!count),
      take(1)
    );
  }

  guardOneCodeFile(id: string): Observable<boolean | UrlTree | any> {
    return this.codeFileService.store.pipe(
      select(this.codeFileService.selectEntityById, { id }),
      // if codeFiles not loaded, dispatch an API action
      tap((codeFiles) => {
        if (!codeFiles) {
          this.codeFileService.getByKey(id);
        }
      }),
      filter((result) => !!result),
      take(1)
    );
  }
}
