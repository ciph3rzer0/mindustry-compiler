import { Timestamp } from '@firebase/firestore-types';

export class Codefile {
  id!: string;
  uid?: string;

  // Redundant user info
  displayName: string;
  photoUrl?: string;
  // type?: string;

  docName: string;
  doc?: string;
  createDate: Timestamp;
  updateDate: Timestamp;
  version: string;
  versionHistory: string;

  // For historical versions in redux store
  parent?: string;
}
