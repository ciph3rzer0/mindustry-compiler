import { Injectable } from '@angular/core';
import {
  EntityCollectionServiceBase,
  EntityCollectionServiceElementsFactory,
} from '@ngrx/data';
import { Dictionary } from '@ngrx/entity';
import { createSelector } from '@ngrx/store';
import { Observable } from 'rxjs';
import { first, map, tap } from 'rxjs/operators';
import { Codefile } from '../models/codefile.model';
import { getRouterState } from '../router';

@Injectable({ providedIn: 'root' })
// @ts-ignore
export class CodefileService extends EntityCollectionServiceBase<Codefile> {
  constructor(serviceElementsFactory: EntityCollectionServiceElementsFactory) {
    super('Codefile', serviceElementsFactory);
  }

  public getAllCodefiles(): Observable<Codefile[]> {
    return this.store.select(this.selectAllCodefiles, {});
  }

  public selectAllCodefiles = createSelector(
    this.selectors.selectEntities,
    (entities: Codefile[], props): Codefile[] => {
      return entities.filter((f: Codefile) => !f.parent);
    }
  );

  /**
   * Get Clip Snapshot By Id
   */
  public getClipSnapshotById(fileId: string): Codefile {
    var ret: Codefile;

    this.entityMap$.pipe(
      map((entities) => entities[fileId]),
      first(),
      tap((c) => (ret = c))
    );

    return ret;
  }

  /**
   *  Retrieve a specific document version for an id.
   */
  public fetchCodefileVersion(id: string, version: string): void {
    this.getWithQuery({ history: id, version });
  }
  public getCodefileVersion(id: string): Observable<Codefile[]> {
    return this.store.select(this.selectCodeFileVersion, id);
  }
  public selectCodeFileVersion = createSelector(
    this.selectors.selectEntities,
    (entities: Codefile[], props): Codefile[] => {
      return entities.filter(
        (f: Codefile) => f.parent == props.id && f.version == props.version
      );
    }
  );

  /**
   *  Retrieve all versioned documents for an id.
   */
  public fetchCodefileHistory(id: string): void {
    this.getWithQuery({ history: id });
  }
  public getCodefileHistory(id: string): Observable<Codefile[]> {
    return this.store.select(this.selectCodeFileHistory, id);
  }
  public selectCodeFileHistory = createSelector(
    this.selectors.selectEntities,
    (entities: Codefile[], id: string): Codefile[] => {
      return entities.filter((f: Codefile) => f.parent == id);
    }
  );

  public getSelectedFromCache(): Observable<Codefile> {
    return this.store.select(this.selectCodeFileByRoute);
  }

  public selectCodeFileByRoute = createSelector(
    this.selectors.selectEntityMap,
    getRouterState,
    (entities: Dictionary<Codefile>, router): Codefile => {
      return entities[router?.state?.params.fileId];
    }
  );

  public selectEntityById = createSelector(
    this.selectors.selectEntityMap,
    (entities, props): Codefile => {
      return entities[props.id];
    }
  );
}
