import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import * as userActions from 'src/app/data/auth/user.actions';
import { User } from 'src/app/data/auth/user.model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
})
export class HeaderComponent implements OnInit {
  title = 'MINDUSTRY LOGIC';
  user$: Observable<User>;

  constructor(private store: Store<{ user: User }>) {}

  ngOnInit(): void {
    this.user$ = this.store.select('user');
  }

  googleLogin() {
    this.store.dispatch(userActions.userGoogleLogin({}));
  }

  logout() {
    this.store.dispatch(userActions.userLogout({}));
  }
}
