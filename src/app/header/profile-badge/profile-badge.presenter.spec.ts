import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { AppModule } from 'src/app/app.module';
import { ProfileBadgePresenter } from './profile-badge.presenter';

describe('ProfileBadgePresenter', () => {
  let component: ProfileBadgePresenter;
  let fixture: ComponentFixture<ProfileBadgePresenter>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [AppModule],
      declarations: [ProfileBadgePresenter],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileBadgePresenter);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
