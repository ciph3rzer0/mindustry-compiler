import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-profile-badge',
  templateUrl: './profile-badge.presenter.html',
  styleUrls: ['./profile-badge.presenter.scss'],
})
export class ProfileBadgePresenter implements OnInit {
  @Input() photoUrl: string;
  @Input() size: number = 2.5;

  constructor() {}

  ngOnInit(): void {}
}
