import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { ClarityModule } from '@clr/angular';
import { VersionSelectWidget, VERSION_ZERO } from './version-select.widget';

describe('VersionSelectWidget', () => {
  let component: VersionSelectWidget;
  let fixture: ComponentFixture<VersionSelectWidget>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [ClarityModule],
      declarations: [VersionSelectWidget],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VersionSelectWidget);
    component = fixture.componentInstance;
    component.version = '1.2.3';
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
    expect(component.newVersion).toEqual({
      major: 1,
      minor: 2,
      patch: 4,
    });
  });

  it('should parse versions', () => {
    // Happy case
    expect(component.splitVersion('3.2.1')).toEqual({
      major: 3,
      minor: 2,
      patch: 1,
    });
  });

  it('shouldnt parse if given the wrong number of integers', () => {
    expect(component.splitVersion('3.2.1.0')).toEqual(VERSION_ZERO);
    expect(component.splitVersion('3.2')).toEqual(VERSION_ZERO);
    expect(component.splitVersion('3')).toEqual(VERSION_ZERO);
    expect(component.splitVersion('')).toEqual(VERSION_ZERO);
  });

  it('shouldnt parse if given blank versions', () => {
    expect(component.splitVersion('.2.1')).toEqual(VERSION_ZERO);
    expect(component.splitVersion('3..1')).toEqual(VERSION_ZERO);
    expect(component.splitVersion('3.2.1.')).toEqual(VERSION_ZERO);
  });

  it('shouldnt parse if given letter characters', () => {
    expect(component.splitVersion('3.2.b')).toEqual(VERSION_ZERO);
  });
});
