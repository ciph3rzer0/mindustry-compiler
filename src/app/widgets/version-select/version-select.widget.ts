import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

type Version = {
  major: number;
  minor: number;
  patch: number;
};

export const VERSION_ZERO = { major: 0, minor: 0, patch: 0 };

@Component({
  selector: 'app-version-select',
  templateUrl: './version-select.widget.html',
  styleUrls: ['./version-select.widget.scss'],
})
export class VersionSelectWidget implements OnInit {
  @Output() output: EventEmitter<string> = new EventEmitter();

  @Input() set version(value: string) {
    this.initialVersion = this.splitVersion(value);
    this.selectedIndex = this._selectedIndex;
  }

  initialVersion: Version;
  newVersion: Version;

  _selectedIndex: number = 2;
  set selectedIndex(value: number) {
    this._selectedIndex = value;

    this.newVersion = this.initialVersion
      ? { ...this.initialVersion }
      : { ...VERSION_ZERO };

    switch (value) {
      case 0:
        this.newVersion.major++;
        this.newVersion.minor = 0;
        this.newVersion.patch = 0;
        break;
      case 1:
        this.newVersion.minor++;
        this.newVersion.patch = 0;
        break;
      default:
        this._selectedIndex = 2;
        this.newVersion.patch++;
    }

    this.output.emit(
      `${this.newVersion.major}.${this.newVersion.minor}.${this.newVersion.patch}`
    );
  }

  ngOnInit(): void {}

  /**
   * Converts a string to a Version type {major, minor, patch}.  All integers.
   * @param string versionStr the string to parse into a Version
   * @return Version representation of versionStr.  VERSION_ZERO if string can't be parsed.
   */
  splitVersion(versionStr: string): Version {
    let version = VERSION_ZERO;
    let arr: Array<any>;

    try {
      arr = versionStr.split('.');
      if (arr.length == 3) {
        arr = arr.map((v: string) => {
          let num = Number(v);
          // console.log(`${v} -> ${num}`);

          if (isNaN(num)) {
            throw new Error(`Not a number: ${v}`);
          } else if (!v) {
            throw new Error(`Blank: ${v}`);
          }

          return num;
        });
      }
    } catch (err) {
      console.log(`Invalid version: "${versionStr}"`);
      console.log(err);
      arr.length = 0;
    }

    if (arr?.length == 3) {
      version = {
        major: arr[0],
        minor: arr[1],
        patch: arr[2],
      };
    }

    return version;
  }
}
