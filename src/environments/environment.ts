// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  emulated: false,
  firebaseConfig: {
    apiKey: 'AIzaSyDPCVVZaV_5NLpgwZImWpYf845ZhyGKHc0',
    authDomain: 'mindustry-compiler.firebaseapp.com',
    projectId: 'mindustry-compiler',
    storageBucket: 'mindustry-compiler.appspot.com',
    messagingSenderId: '801175619479',
    appId: '1:801175619479:web:581bb2706f0e002fe2e6d2',
    measurementId: 'G-BV9N6YYMSY',
  },
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
